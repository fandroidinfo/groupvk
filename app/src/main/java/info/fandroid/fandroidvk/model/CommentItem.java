package info.fandroid.fandroidvk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import info.fandroid.fandroidvk.model.attachment.ApiAttachment;
import info.fandroid.fandroidvk.model.attachment.Attachment;
import info.fandroid.fandroidvk.model.countable.Likes;
import info.fandroid.fandroidvk.model.countable.Reposts;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by wetalkas on 7/11/16.
 */
public class CommentItem extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("from_id")
    @Expose
    public int senderId;

    @SerializedName("place")
    @Expose
    public Place place;


    public String senderName;

    public String senderPhoto;

    @SerializedName("date")
    @Expose
    public int date;
    @SerializedName("text")
    @Expose
    public String text;

    @SerializedName("attachments")
    @Expose
    public RealmList<ApiAttachment> attachments = new RealmList<>();

    public String attachmentsString;

    @SerializedName("likes")
    @Expose
    public Likes likes;


    @SerializedName("reposts")
    @Expose
    public Reposts reposts;

    public boolean isFromTopic = false;


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getFromId() {
        return senderId;
    }

    public void setFromId(int fromId) {
        this.senderId = fromId;
    }

    public Integer getDate() {
        return date;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getDisplayText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public RealmList<ApiAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(RealmList<ApiAttachment> attachments) {
        this.attachments = attachments;
    }


    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public Reposts getReposts() {
        return reposts;
    }

    public void setReposts(Reposts reposts) {
        this.reposts = reposts;
    }


    public String getDisplayAttachmentsString() {
        return attachmentsString;
    }

    public List<Attachment> getDisplayAttachments() {
        return getParsedAttachments();
    }

    public List<Attachment> getParsedAttachments() {
        List<Attachment> attachments = new ArrayList<>();
        for (ApiAttachment apiAttachment : getAttachments()) {
            attachments.add(apiAttachment.getAttachment());
        }
        return attachments;
    }

    public void setAttachmentsString(String attachmentsString) {
        this.attachmentsString = attachmentsString;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getSenderPhoto() {
        return senderPhoto;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public void setSenderPhoto(String senderPhoto) {
        this.senderPhoto = senderPhoto;
    }

    public String getFullName() {
        return senderName;
    }

    public String getPhoto() {
        return senderPhoto;
    }


    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }


    public boolean isFromTopic() {
        return isFromTopic;
    }

    public void setIsFromTopic(boolean isTopic) {
        this.isFromTopic = isTopic;
    }
}