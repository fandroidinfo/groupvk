package info.fandroid.fandroidvk.model;

/**
 * Created by wetalkas on 7/1/16.
 */
public interface Identifiable {
    String ID = "id";

    int getId();
}
