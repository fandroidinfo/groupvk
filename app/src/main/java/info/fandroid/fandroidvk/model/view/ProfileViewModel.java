package info.fandroid.fandroidvk.model.view;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.model.owner.Member;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;

/**
 * Created by wet on 22.07.17.
 */

public class ProfileViewModel extends BaseViewModel {

    public int id;

    public int groupId;

    public String photo;

    private String mFullName;

    public ProfileViewModel() {

    }

    public ProfileViewModel(Member member) {
        this.id = member.getId();
        this.groupId = member.getGroupId();

        this.photo = member.getPhoto();
        this.mFullName = member.getFullName();
    }

    @Override
    public LayoutTypes getType() {
        return LayoutTypes.Member;
    }

    @Override
    protected BaseViewHolder onCreateViewHolder(View view) {
        return new ProfileViewHolder(view);
    }


    static class ProfileViewHolder extends BaseViewHolder<ProfileViewModel> {

        @BindView(R.id.civ_profile_image)
        public CircleImageView civProfilePhoto;

        @BindView(R.id.tv_profile_name)
        public TextView civProfileName;


        public ProfileViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindViewHolder(ProfileViewModel profileViewModel) {
            Context context = itemView.getContext();

            Glide.with(context)
                    .load(profileViewModel.getPhoto())
                    .into(civProfilePhoto);
            civProfileName.setText(profileViewModel.getFullName());
        }



        @Override
        public void unbindViewHolder() {
            civProfileName.setText(null);
            civProfilePhoto.setImageBitmap(null);
        }



    }


    public int getId() {
        return id;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getPhoto() {
        return photo;
    }

    public String getFullName() {
        return mFullName;
    }
}
