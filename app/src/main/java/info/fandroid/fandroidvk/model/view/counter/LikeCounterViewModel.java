package info.fandroid.fandroidvk.model.view.counter;

import info.fandroid.fandroidvk.model.countable.Likes;

/**
 * Created by wet on 01.05.17.
 */

public class LikeCounterViewModel extends CounterViewModel {

    private Likes mLikes;

    public LikeCounterViewModel(Likes likes) {
        super(likes.getCount());

        this.mLikes = likes;

        if (mLikes.isUserLikes()) {
            setAccentColor();
        }
    }

    public Likes getLikes() {
        return mLikes;
    }


}
