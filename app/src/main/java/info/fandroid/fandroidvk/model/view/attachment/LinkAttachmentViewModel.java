package info.fandroid.fandroidvk.model.view.attachment;

import android.view.View;

import info.fandroid.fandroidvk.model.attachment.Link;
import info.fandroid.fandroidvk.model.view.BaseViewModel;
import info.fandroid.fandroidvk.ui.view.holder.attachment.LinkAttachmentViewHolder;

/**
 * Created by wet on 24.03.17.
 */

public class LinkAttachmentViewModel extends BaseViewModel {

    private String mTitle;
    private String mUrl;

    public LinkAttachmentViewModel(Link link) {

        if (link.getTitle() == null || link.getTitle().equals("")) {
            if (link.getName() != null) {
                mTitle = link.getName();
            } else {
                mTitle = "Link";
            }
        } else {
            mTitle = link.getTitle();
        }

        mUrl = link.getUrl();
    }


    @Override
    public LayoutTypes getType() {
        return LayoutTypes.AttachmentLink;
    }

    @Override
    public LinkAttachmentViewHolder onCreateViewHolder(View view) {
        return new LinkAttachmentViewHolder(view);
    }


    public String getTitle() {
        return mTitle;
    }

    public String getUrl() {
        return mUrl;
    }
}
