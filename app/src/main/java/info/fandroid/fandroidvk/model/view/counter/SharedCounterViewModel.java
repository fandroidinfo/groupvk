package info.fandroid.fandroidvk.model.view.counter;

import info.fandroid.fandroidvk.model.countable.Reposts;

/**
 * Created by wet on 01.05.17.
 */

public class SharedCounterViewModel extends CounterViewModel {

    private Reposts mReposts;

    public SharedCounterViewModel(Reposts reposts) {
        super(reposts.getCount());

        this.mReposts = reposts;
        if (mReposts.isUserReposted()) {
            setAccentColor();
        }
    }

    public Reposts getReposts() {
        return mReposts;
    }
}
