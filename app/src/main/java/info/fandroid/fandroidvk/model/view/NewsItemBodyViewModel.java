package info.fandroid.fandroidvk.model.view;

import android.view.View;

import info.fandroid.fandroidvk.model.WallItem;
import info.fandroid.fandroidvk.ui.view.holder.NewsItemBodyHolder;

/**
 * Created by wet on 10.04.17.
 */

public class NewsItemBodyViewModel extends BaseViewModel
        implements View.OnClickListener{

    private int mId;

    private String mText;
    private String mAttachmentsString;

    private boolean mIsRepost;

    public NewsItemBodyViewModel(WallItem wallItem) {
        this.mId = wallItem.getId();

        this.mIsRepost = wallItem.haveSharedRepost();

        if (mIsRepost) {
            this.mText = wallItem.getSharedRepost().getText();
            this.mAttachmentsString = wallItem.getSharedRepost().getAttachmentsString();
        } else {
            this.mText = wallItem.getText();
            this.mAttachmentsString = wallItem.getAttachmentsString();
        }
    }


    @Override
    public boolean isItemDecorator() {
        return true;
    }

    @Override
    public LayoutTypes getType() {
        return LayoutTypes.NewsFeedItemBody;
    }

    @Override
    public NewsItemBodyHolder onCreateViewHolder(View view) {
        return new NewsItemBodyHolder(view);
    }


    @Override
    public void onClick(View view) {
//        OpenedPostFragment openedPostFragment = new OpenedPostFragment();
//        Bundle args = new Bundle();
//        args.putInt("id", mId);
//        openedPostFragment.setArguments(args);
//
//        ((BaseFragmentActivity) view.getContext()).getFragmentController().showFragment(openedPostFragment);
    }


    public String getText() {
        return mText;
    }

    public int getId() {
        return mId;
    }

    public String getAttachmentsString() {
        return mAttachmentsString;
    }
}