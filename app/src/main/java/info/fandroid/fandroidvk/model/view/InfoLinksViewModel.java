package info.fandroid.fandroidvk.model.view;

import android.view.View;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;

/**
 * Created by wet on 12.04.17.
 */

public class InfoLinksViewModel extends BaseViewModel {


    @Override
    public LayoutTypes getType() {
        return LayoutTypes.InfoLinks;
    }

    @Override
    public InfoLinksViewHolder onCreateViewHolder(View view) {
        return new InfoLinksViewHolder(view);
    }


    static class InfoLinksViewHolder extends BaseViewHolder<InfoLinksViewModel>
            implements View.OnClickListener{


        @BindView(R.id.rv_links)
        RelativeLayout rvLinks;

        public InfoLinksViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindViewHolder(InfoLinksViewModel infoLinksViewModel) {
            rvLinks.setOnClickListener(this);
        }

        @Override
        public void unbindViewHolder() {
            rvLinks.setOnClickListener(null);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.rv_links:
                    //UiNavigation.openGroupLinks((BaseFragmentActivity) view.getContext());
                    break;
            }
        }
    }
}
