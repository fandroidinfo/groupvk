package info.fandroid.fandroidvk.model.view;

import android.view.View;

import info.fandroid.fandroidvk.model.WallItem;
import info.fandroid.fandroidvk.model.view.counter.CommentsCounterViewModel;
import info.fandroid.fandroidvk.model.view.counter.LikeCounterViewModel;
import info.fandroid.fandroidvk.model.view.counter.SharedCounterViewModel;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;
import info.fandroid.fandroidvk.ui.view.holder.NewsItemFooterHolder;

/**
 * Created by wet on 10.04.17.
 */

public class NewsItemFooterViewModel extends BaseViewModel {

    private int mId;
    private int ownerId;

    private long mDateLong;

    private LikeCounterViewModel mLikes;
    private CommentsCounterViewModel mComments;
    private SharedCounterViewModel mReposts;


    public NewsItemFooterViewModel(WallItem wallItem) {
        this.mId = wallItem.getId();
        this.ownerId = wallItem.getOwnerId();

        this.mDateLong = wallItem.getDate();

        this.mLikes = new LikeCounterViewModel(wallItem.getLikes());
        this.mComments = new CommentsCounterViewModel(wallItem.getComments());
        this.mReposts = new SharedCounterViewModel(wallItem.getReposts());
    }

    @Override
    public boolean isItemDecorator() {
        return true;
    }

    @Override
    public BaseViewModel.LayoutTypes getType() {
        return LayoutTypes.NewsFeedItemFooter;
    }

    @Override
    protected BaseViewHolder onCreateViewHolder(View view) {
        return new NewsItemFooterHolder(view);
    }

    public int getId() {
        return mId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public long getDateLong() {
        return mDateLong;
    }

    public LikeCounterViewModel getLikes() {
        return mLikes;
    }

    public CommentsCounterViewModel getComments() {
        return mComments;
    }

    public SharedCounterViewModel getReposts() {
        return mReposts;
    }

    public void setmLikes(LikeCounterViewModel mLikes) {
        this.mLikes = mLikes;
    }
}