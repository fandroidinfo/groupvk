package info.fandroid.fandroidvk.model.view;

import android.view.View;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;

/**
 * Created by wet on 12.04.17.
 */

public class InfoContactsViewModel extends BaseViewModel {

    @Override
    public LayoutTypes getType() {
        return LayoutTypes.InfoContacts;
    }

    @Override
    public InfoContactsViewHolder onCreateViewHolder(View view) {
        return new InfoContactsViewHolder(view);
    }


    static class InfoContactsViewHolder extends BaseViewHolder<InfoContactsViewModel>
            implements View.OnClickListener {
        @BindView(R.id.rv_contacts)
        RelativeLayout rvContacts;

        public InfoContactsViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindViewHolder(InfoContactsViewModel infoContactsViewModel) {
            rvContacts.setOnClickListener(this);
        }

        @Override
        public void unbindViewHolder() {
            rvContacts.setOnClickListener(null);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.rv_contacts:
                    //UiNavigation.openContacts((BaseFragmentActivity) view.getContext());
                    break;
            }
        }
    }
}
