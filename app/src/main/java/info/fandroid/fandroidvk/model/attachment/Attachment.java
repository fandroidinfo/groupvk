package info.fandroid.fandroidvk.model.attachment;

import info.fandroid.fandroidvk.model.Identifiable;

/**
 * Created by wet on 15.10.16.
 */

public interface Attachment extends Identifiable {
    String getType();
}
