package info.fandroid.fandroidvk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by wet on 27.10.16.
 */

public class PostSource extends RealmObject {

    @SerializedName("type")
    @Expose
    public String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}