package info.fandroid.fandroidvk.model.attachment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vk.sdk.api.model.VKAttachments;

import io.realm.RealmObject;

/**
 * Created by wetalkas on 6/30/16.
 */
public class Photo extends RealmObject implements Attachment {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("album_id")
    @Expose
    public int albumId;

    @SerializedName("owner_id")
    @Expose
    public int ownerId;

    @SerializedName("photo_75")
    @Expose
    public String photo75;

    @SerializedName("photo_130")
    @Expose
    public String photo130;

    @SerializedName("photo_604")
    @Expose
    public String photo604;

    @SerializedName("photo_807")
    @Expose
    public String photo807;

    @SerializedName("photo_1280")
    @Expose
    public String photo1280;

    @SerializedName("width")
    @Expose
    public int width;

    @SerializedName("height")
    @Expose
    public int height;

    @SerializedName("text")
    @Expose
    public String text;

    @SerializedName("date")
    @Expose
    public int date;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public String getPhoto75() {
        return photo75;
    }

    public void setPhoto75(String photo75) {
        this.photo75 = photo75;
    }

    public String getPhoto130() {
        return photo130;
    }

    public void setPhoto130(String photo130) {
        this.photo130 = photo130;
    }

    public String getPhoto604() {
        return photo604;
    }

    public void setPhoto604(String photo604) {
        this.photo604 = photo604;
    }

    public String getPhoto807() {
        return photo807;
    }

    public void setPhoto807(String photo807) {
        this.photo807 = photo807;
    }

    public String getPhoto1280() {
        return photo1280;
    }

    public void setPhoto1280(String photo1280) {
        this.photo1280 = photo1280;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    @Override
    public String getType() {
        return VKAttachments.TYPE_PHOTO;
    }
}
