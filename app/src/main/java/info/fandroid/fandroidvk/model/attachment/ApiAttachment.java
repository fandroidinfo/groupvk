package info.fandroid.fandroidvk.model.attachment;

import com.vk.sdk.api.model.VKAttachments;

import java.util.NoSuchElementException;

import info.fandroid.fandroidvk.model.attachment.doc.Doc;
import info.fandroid.fandroidvk.model.attachment.video.Video;
import io.realm.RealmObject;

/**
 * Created by wetalkas on 6/30/16.
 */
public class ApiAttachment extends RealmObject {

    public String type;
    public Photo photo;
    public Audio audio;
    public Video video;
    public Doc doc;
    public Link link;
    public Page page;

    public Attachment getAttachment() {
        switch (type) {
            case VKAttachments.TYPE_PHOTO:
                return photo;
            case VKAttachments.TYPE_AUDIO:
                return audio;
            case VKAttachments.TYPE_VIDEO:
                return video;
            case VKAttachments.TYPE_DOC:
                return doc;
            case VKAttachments.TYPE_LINK:
                return link;
            case VKAttachments.TYPE_WIKI_PAGE:
                return page;
            default:
                throw new NoSuchElementException("Attachment type " + type + " is not supported.");
        }
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public Doc getDoc() {
        return doc;
    }

    public void setDoc(Doc doc) {
        this.doc = doc;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Link getLink() {
        return link;
    }

    public Page getPage() {
        return page;
    }


}

