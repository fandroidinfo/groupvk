package info.fandroid.fandroidvk.model.attachment.doc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vk.sdk.api.model.VKAttachments;

import info.fandroid.fandroidvk.model.attachment.Attachment;
import io.realm.RealmObject;

/**
 * Created by wetalkas on 7/4/16.
 */
public class Doc extends RealmObject implements Attachment {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("owner_id")
    @Expose
    public int ownerId;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("size")
    @Expose
    public int size;
    @SerializedName("ext")
    @Expose
    public String ext;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("date")
    @Expose
    public int date;
    @SerializedName("type")
    @Expose
    public int mtype;
    @SerializedName("preview")
    @Expose
    public Preview preview;
    @SerializedName("access_key")
    @Expose
    public String accessKey;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getMType() {
        return mtype;
    }

    public void setMtype(int mtype) {
        this.mtype = mtype;
    }

    @Override
    public String getType() {
        return VKAttachments.TYPE_DOC;
    }

    public Preview getPreview() {
        return preview;
    }

    public void setPreview(Preview preview) {
        this.preview = preview;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

}
