package info.fandroid.fandroidvk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import info.fandroid.fandroidvk.model.attachment.ApiAttachment;
import info.fandroid.fandroidvk.model.attachment.Attachment;
import info.fandroid.fandroidvk.model.countable.Comments;
import info.fandroid.fandroidvk.model.countable.Likes;
import info.fandroid.fandroidvk.model.countable.Reposts;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by wetalkas on 6/30/16.
 */

public class WallItem extends RealmObject implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("from_id")
    @Expose
    public int senderId;

    @SerializedName("sender_name")
    @Expose
    public String senderName;

    @SerializedName("sender_photo")
    @Expose
    public String senderPhoto;

    @SerializedName("owner_id")
    @Expose
    public int ownerId;

    @SerializedName("date")
    @Expose
    public int date;

    @SerializedName("post_type")
    @Expose
    public String postType;

    @SerializedName("text")
    @Expose
    public String text;

    @SerializedName("is_pinned")
    @Expose
    public int isPinned;

    @SerializedName("attachments")
    @Expose
    public RealmList<ApiAttachment> attachments = new RealmList<>();

    @SerializedName("comments")
    @Expose
    public Comments comments;

    @SerializedName("likes")
    public Likes likes;

    @SerializedName("reposts")
    @Expose
    public Reposts reposts;

    @SerializedName("copy_history")
    @Expose
    public RealmList<WallItem> copyHistory = new RealmList<>();

    @SerializedName("post_source")
    @Expose
    public PostSource postSource;

    @SerializedName("attachments_string")
    @Expose
    public String attachmentsString;


    @SerializedName("is_repost")
    @Expose
    public boolean isRepost;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderPhoto() {
        return senderPhoto;
    }

    public void setSenderPhoto(String senderPhoto) {
        this.senderPhoto = senderPhoto;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }


    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getIsPinned() {
        return isPinned;
    }

    public void setIsPinned(int isPinned) {
        this.isPinned = isPinned;
    }

    public RealmList<ApiAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(RealmList<ApiAttachment> attachments) {
        this.attachments = attachments;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public Reposts getReposts() {
        return reposts;
    }

    public void setReposts(Reposts reposts) {
        this.reposts = reposts;
    }

    public RealmList<WallItem> getCopyHistory() {
        return copyHistory;
    }

    public void setCopyHistory(RealmList<WallItem> copyHistory) {
        this.copyHistory = copyHistory;
    }

    public PostSource getPostSource() {
        return postSource;
    }

    public void setPostSource(PostSource postSource) {
        this.postSource = postSource;
    }

    public String getAttachmentsString() {
        return attachmentsString;
    }

    public void setAttachmentsString(String attachmentsString) {
        this.attachmentsString = attachmentsString;
    }

    public boolean isRepost() {
        return isRepost;
    }

    public void setIsRepost(boolean repost) {
        isRepost = repost;
    }

    public boolean haveSharedRepost() {
        return getCopyHistory().size() > 0;
    }

    public WallItem getSharedRepost() {
        if (haveSharedRepost()) {
            return getCopyHistory().first();
        }

        return null;
    }

    public List<Attachment> getParsedAttachments() {
        List<Attachment> attachments = new ArrayList<>();
        List<ApiAttachment> apiAttachments = haveSharedRepost() ? getSharedRepost().getAttachments() : getAttachments();
        for (ApiAttachment apiAttachment : apiAttachments) {
            attachments.add(apiAttachment.getAttachment());
        }
        return attachments;
    }
}
