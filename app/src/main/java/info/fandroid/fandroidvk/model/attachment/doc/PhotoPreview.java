package info.fandroid.fandroidvk.model.attachment.doc;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by wet on 27.10.16.
 */

public class PhotoPreview extends RealmObject {
    RealmList<Size> sizes;

    public RealmList<Size> getSizes() {
        return sizes;
    }

    public void setSizes(RealmList<Size> sizes) {
        this.sizes = sizes;
    }
}