package info.fandroid.fandroidvk.model.countable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by wetalkas on 6/30/16.
 */
public class Reposts extends RealmObject implements Countable {
    public int count;

    @SerializedName("user_reposted")
    public int userResposted;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isUserReposted() {
        return userResposted == 1;
    }

    public int getUserResposted() {
        return userResposted;
    }

    public void setUserResposted(int userResposted) {
        this.userResposted = userResposted;
    }
}
