package info.fandroid.fandroidvk.model.view.counter;

import info.fandroid.fandroidvk.model.countable.Comments;

/**
 * Created by wet on 01.05.17.
 */

public class CommentsCounterViewModel extends CounterViewModel {

    private Comments mComments;

    public CommentsCounterViewModel(Comments comments) {
        super(comments.getCount());

        this.mComments = comments;
    }

    public Comments getComments() {
        return mComments;
    }
}
