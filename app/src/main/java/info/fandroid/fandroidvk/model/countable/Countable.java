package info.fandroid.fandroidvk.model.countable;

/**
 * Created by wet on 29.10.16.
 */

public interface Countable {
    int getCount();
}
