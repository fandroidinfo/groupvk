package info.fandroid.fandroidvk.model.attachment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vk.sdk.api.model.VKAttachments;

import io.realm.RealmObject;

/**
 * Created by wetalkas on 7/2/16.
 */
public class Audio extends RealmObject implements Attachment {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("owner_id")
    @Expose
    public int ownerId;
    @SerializedName("artist")
    @Expose
    public String artist;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("duration")
    @Expose
    private int duration;
    @SerializedName("date")
    @Expose
    public int date;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("lyrics_id")
    @Expose
    public int lyricsId;
    @SerializedName("genre_id")
    @Expose
    public int genreId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getLyricsId() {
        return lyricsId;
    }

    public void setLyricsId(int lyricsId) {
        this.lyricsId = lyricsId;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    @Override
    public String getType() {
        return VKAttachments.TYPE_AUDIO;
    }
}
