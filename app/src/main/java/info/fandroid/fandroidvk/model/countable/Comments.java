package info.fandroid.fandroidvk.model.countable;

import io.realm.RealmObject;

/**
 * Created by wetalkas on 6/30/16.
 */
public class Comments extends RealmObject implements Countable{
    public int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
