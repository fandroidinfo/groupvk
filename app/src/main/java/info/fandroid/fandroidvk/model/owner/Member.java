package info.fandroid.fandroidvk.model.owner;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by wet on 08.01.17.
 */

public class Member extends RealmObject {

    public static final String ID = "id";
    public static final String GROUP_ID = "group_id";
    public static final String PHOTO = "photo_100";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String GROUP_CONTANCT = "group_contact";

    @PrimaryKey
    @SerializedName(ID)
    public int id;

    @SerializedName(GROUP_ID)
    public int groupId;

    @SerializedName(PHOTO)
    public String photo;

    @SerializedName(FIRST_NAME)
    public String firstName;

    @SerializedName(LAST_NAME)
    public String lastName;

    @SerializedName(GROUP_CONTANCT)
    public boolean groupContact;


    public Member() {

    }

    public Member(Profile profile) {
        this.id = profile.id;
        this.photo = profile.getPhoto();
        this.firstName = profile.firstName;
        this.lastName = profile.lastName;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public boolean isGroupContact() {
        return groupContact;
    }

    public void setGroupContact(boolean groupContact) {
        this.groupContact = groupContact;
    }


}
