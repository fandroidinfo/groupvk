package info.fandroid.fandroidvk.model.attachment.video;

import io.realm.RealmObject;

/**
 * Created by wet on 27.10.16.
 */

public class File extends RealmObject {
    public String external;

    public String getExternal() {
        return external;
    }

    public void setExternal(String external) {
        this.external = external;
    }
}