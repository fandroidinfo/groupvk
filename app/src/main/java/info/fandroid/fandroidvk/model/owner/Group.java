package info.fandroid.fandroidvk.model.owner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import info.fandroid.fandroidvk.model.Contact;
import info.fandroid.fandroidvk.model.attachment.Link;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by wetalkas on 6/30/16.
 */
public class Group extends RealmObject implements Owner {

    public static final String CONTACTS_IDS = "contacts";
    public static final String CONTACTS_LIST = "contacts_list";

    @PrimaryKey
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("photo_50")
    @Expose
    public String photo50;

    @SerializedName("photo_100")
    @Expose
    public String photo100;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("screen_name")
    @Expose
    public String screenName;
    @SerializedName("is_closed")
    @Expose
    public int isClosed;
    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("site")
    @Expose
    public String site;

    @SerializedName("links")
    RealmList<Link> links;

    @SerializedName("contacts")
    public RealmList<Contact> contactsList;

    @SerializedName("photo_200")
    @Expose
    public String photo200;

    public boolean isGroup() {
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return name;
    }

    @Override
    public String getPhoto() {
        return photo100;
    }

    public String getDisplayProfilePhoto() {
        return photo100;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public int getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(int isClosed) {
        this.isClosed = isClosed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoto50() {
        return photo50;
    }

    public void setPhoto50(String photo50) {
        this.photo50 = photo50;
    }

    public void setPhoto100(String photo100) {
        this.photo100 = photo100;
    }

    public String getPhoto200() {
        return photo200;
    }

    public void setPhoto200(String photo200) {
        this.photo200 = photo200;
    }

    public String getPhoto100() {
        return photo100;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public RealmList<Link> getLinks() {
        return links;
    }

    public void setLinks(RealmList<Link> links) {
        this.links = links;
    }


    public RealmList<Contact> getContactsList() {
        return contactsList;
    }

    public void setContactsList(RealmList<Contact> contactsList) {
        this.contactsList = contactsList;
    }
}
