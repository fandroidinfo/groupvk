package info.fandroid.fandroidvk.model;

import android.support.annotation.AnimRes;

/**
 * Created by wet on 14.07.17.
 */
public class MyAnimation {
    private int enter;
    private int exit;

    public MyAnimation() {

    }

    public MyAnimation(@AnimRes int enter, @AnimRes int exit) {
        this.enter = enter;
        this.exit = exit;
    }

    public int getEnter() {
        return enter;
    }

    public void setEnter(int enter) {
        this.enter = enter;
    }

    public int getExit() {
        return exit;
    }

    public void setExit(int exit) {
        this.exit = exit;
    }
}
