package info.fandroid.fandroidvk.model.owner;

import info.fandroid.fandroidvk.model.Identifiable;

/**
 * Created by wet on 09.10.16.
 */

public interface Owner extends Identifiable {
    String getFullName();
    String getPhoto();
}
