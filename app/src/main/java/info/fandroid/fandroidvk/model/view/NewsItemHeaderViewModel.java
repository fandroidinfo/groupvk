package info.fandroid.fandroidvk.model.view;

import android.view.View;

import info.fandroid.fandroidvk.model.WallItem;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;
import info.fandroid.fandroidvk.ui.view.holder.PostHeaderViewHolder;

/**
 * Created by wet on 10.04.17.
 */

public class NewsItemHeaderViewModel extends BaseViewModel {
    private int mId;

    private String mProfilePhoto;
    private String mProfileName;

    private boolean mIsRepost;
    private String mRepostProfileName;


    public NewsItemHeaderViewModel(WallItem wallItem) {
        this.mId = wallItem.getId();

        this.mIsRepost = wallItem.haveSharedRepost();

        if (mIsRepost) {
            this.mRepostProfileName = wallItem.getSharedRepost().senderName;
        }

        this.mProfilePhoto = wallItem.getSenderPhoto();
        this.mProfileName = wallItem.getSenderName();
    }


    @Override
    public LayoutTypes getType() {
        return LayoutTypes.NewsFeedItemHeader;
    }

    @Override
    protected BaseViewHolder onCreateViewHolder(View view) {
        return new PostHeaderViewHolder(view);
    }


    public int getId() {
        return mId;
    }

    public String getProfilePhoto() {
        return mProfilePhoto;
    }

    public String getProfileName() {
        return mProfileName;
    }

    public boolean isRepost() {
        return mIsRepost;
    }

    public String getRepostProfileName() {
        return mRepostProfileName;
    }
}
