package info.fandroid.fandroidvk.model.view.attachment;

import android.view.View;

import info.fandroid.fandroidvk.model.attachment.Page;
import info.fandroid.fandroidvk.model.view.BaseViewModel;
import info.fandroid.fandroidvk.ui.view.holder.attachment.PageAttachmentHolder;

/**
 * Created by wet on 24.03.17.
 */

public class PageAttachmentViewModel extends BaseViewModel {

    private String mTitle;
    private String mUrl;

    public PageAttachmentViewModel(Page page) {
        mUrl = page.getUrl();
        mTitle = page.getTitle();
    }

    public String getTitle() {
        return mTitle;
    }


    @Override
    public LayoutTypes getType() {
        return LayoutTypes.AttachmentPage;
    }

    @Override
    public PageAttachmentHolder onCreateViewHolder(View view) {
        return new PageAttachmentHolder(view);
    }

    public String getmUrl() {
        return mUrl;
    }
}
