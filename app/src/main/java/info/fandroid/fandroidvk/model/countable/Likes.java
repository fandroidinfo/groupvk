package info.fandroid.fandroidvk.model.countable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by wetalkas on 6/30/16.
 */
public class Likes extends RealmObject implements Countable {

    @SerializedName("count")
    public int count;

    @Expose
    @SerializedName("user_likes")
    public int userLikes;

    @SerializedName("can_like")
    public int canLike;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isUserLikes () {
        return userLikes == 1;
    }

    public void setUserLikes(boolean userLikes) {
        this.userLikes = userLikes ? 1 : 0;
    }

}

