package info.fandroid.fandroidvk.mvp.view;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import info.fandroid.fandroidvk.model.view.BaseViewModel;

/**
 * Created by wet on 15.07.17.
 */

public interface BaseFeedView extends MvpView {
    void showRefreshing();

    void hideRefreshing();


    void showListProgress();

    void hideListProgress();


    void onStartLoading();

    void onFinishLoading();


    void showError(String message);


    void showEmptyScreen();

    void hideEmptyScreen();


    void setItems(List<BaseViewModel> items);

    void addItems(List<BaseViewModel> items);
}
