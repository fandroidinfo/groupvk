package info.fandroid.fandroidvk.mvp.view;

import info.fandroid.fandroidvk.model.view.NewsItemFooterViewModel;

/**
 * Created by wet on 22.07.17.
 */

public interface OpenedPostView extends BaseFeedView {
    void setFooter(NewsItemFooterViewModel viewModel);
}
