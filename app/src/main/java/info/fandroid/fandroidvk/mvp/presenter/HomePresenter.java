package info.fandroid.fandroidvk.mvp.presenter;

import android.content.Intent;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import info.fandroid.fandroidvk.CurrentUser;
import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.common.manager.MyFragmentManager;
import info.fandroid.fandroidvk.common.manager.NetworkManager;
import info.fandroid.fandroidvk.common.util.RxUtils;
import info.fandroid.fandroidvk.model.owner.Profile;
import info.fandroid.fandroidvk.mvp.view.HomeView;
import info.fandroid.fandroidvk.rest.api.UsersApi;
import info.fandroid.fandroidvk.rest.model.request.UsersGetRequestModel;
import info.fandroid.fandroidvk.ui.activity.HomeActivity;
import info.fandroid.fandroidvk.ui.fragment.BaseFragment;
import info.fandroid.fandroidvk.ui.fragment.BoardFragment;
import info.fandroid.fandroidvk.ui.fragment.GroupRulesFragment;
import info.fandroid.fandroidvk.ui.fragment.InfoFragment;
import info.fandroid.fandroidvk.ui.fragment.MembersFragment;
import info.fandroid.fandroidvk.ui.fragment.MyPostsFragment;
import info.fandroid.fandroidvk.ui.fragment.NewsFeedFragment;
import info.fandroid.fandroidvk.ui.fragment.SettingActivity;
import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by wet on 15.07.17.
 */
@InjectViewState
public class HomePresenter extends MvpPresenter<HomeView> {

    @Inject
    UsersApi mUserApi;

    @Inject
    MyFragmentManager myFragmentManager;

    @Inject
    NetworkManager mNetworkManager;

    public HomePresenter() {
        MyApplication.getApplicationComponent().inject(this);
    }

    public void checkAuth() {
        if (!CurrentUser.isAuthorized()) {
            getViewState().startSignIn();
        } else {
            getCurrentUser();
            getViewState().signedIn();
        }
    }

    public void authError() {
        getViewState().showScreenError("Auth error");
    }

    public void drawerItemClick(int id) {
        BaseFragment fragment = null;

        switch (id) {
            case 1:
                fragment = new NewsFeedFragment();
                break;
            case 2:
                fragment = new MyPostsFragment();
                break;
            case 3:
                getViewState().startActivityFromDrawer(SettingActivity.class);
                return;
            case 4:
                fragment = new MembersFragment();
                break;
            case 5:
                fragment = new BoardFragment();
                break;
            case 6:
                fragment = new InfoFragment();
                break;
            case 7:
                fragment = new GroupRulesFragment();
                break;
        }

        if (fragment != null && !myFragmentManager.isAlreadyContains(fragment)) {
            getViewState().showFragmentFromDrawer(fragment);
        }
    }

    private void getCurrentUser() {
        mNetworkManager.getNetworkObservable()
                .flatMap(aBoolean -> {
                    if (!CurrentUser.isAuthorized()) {
                        getViewState().startSignIn();
                    }

                    return aBoolean
                            ? getProfileFromNetwork()
                            : getProfileFromDb();
                })
                .compose(RxUtils.applySchedulers())
                .subscribe(profile -> {
                    getViewState().showCurrentUser(profile);
                }, error -> {
                    error.printStackTrace();
                });
    }


    public void saveToDb(RealmObject item) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> realm1.copyToRealmOrUpdate(item));
    }


    public Observable<Profile> getProfileFromNetwork() {
        return mUserApi.get(new UsersGetRequestModel(CurrentUser.getId()).toMap())
                .flatMap(listFull -> Observable.fromIterable(listFull.response))
                .doOnNext(this::saveToDb);
    }

    private Observable<Profile> getProfileFromDb() {
        return Observable.fromCallable(getListFromRealmCallable());
    }


    public Callable<Profile> getListFromRealmCallable() {
        return () -> {
            Realm realm = Realm.getDefaultInstance();
            Profile realmResults = realm.where(Profile.class)
                    .equalTo("id", Integer.parseInt(CurrentUser.getId()))
                    .findFirst();
            return realm.copyFromRealm(realmResults);
        };
    }
}
