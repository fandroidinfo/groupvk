package info.fandroid.fandroidvk.mvp.presenter;

import com.arellomobile.mvp.MvpPresenter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import info.fandroid.fandroidvk.CurrentUser;
import info.fandroid.fandroidvk.common.manager.NetworkManager;
import info.fandroid.fandroidvk.common.util.RxUtils;
import info.fandroid.fandroidvk.model.view.BaseViewModel;
import info.fandroid.fandroidvk.mvp.view.BaseFeedView;
import io.reactivex.Observable;
import io.reactivex.SingleTransformer;
import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by wet on 15.07.17.
 */
public abstract class BaseFeedPresenter<V extends BaseFeedView> extends MvpPresenter<V> {

    private static final String TAG = "BaseFeedPresenter";

    @Inject
    NetworkManager mNetworkManager;

    public static final int START_PAGE_SIZE = 15;
    public static final int NEXT_PAGE_SIZE = 5;

    private boolean mIsInLoading;

    private int delay = 300;


    public void loadData(ProgressType progressType, int offset, int count) {
        if (mIsInLoading) {
            return;
        }
        mIsInLoading = true;



        mNetworkManager.getNetworkObservable()
                .flatMap(aBoolean -> {
                    if (!aBoolean && offset > 0) {
                        return Observable.empty();
                    }

                    if (!CurrentUser.isAuthorized()) {

                    }

                    return aBoolean
                            ? onCreateLoadDataObservable(count, offset)
                            : onCreateRestoreDataObservable();
                })
                .toList()
                .compose(applyDelay(progressType))
                .compose(RxUtils.applySchedulersSingle())
                .doOnSubscribe(disposable -> {
                    onLoadingStart(progressType);
                })
                .doFinally(() -> {
                    onLoadingFinish(progressType);
                })
                .subscribe(repositories -> {
                    onLoadingSuccess(progressType, repositories);
                }, error -> {
                    error.printStackTrace();
                    onLoadingFailed(error);
                });
    }


    public <T> SingleTransformer<T, T> applyDelay(ProgressType progressType) {
        if (progressType == ProgressType.ListProgress) {
            return tObservable -> tObservable.delaySubscription(delay, TimeUnit.MILLISECONDS);
        }
        return tObservable -> tObservable;
    }


    public abstract Observable<BaseViewModel> onCreateLoadDataObservable(int count, int offset);

    public abstract Observable<BaseViewModel> onCreateRestoreDataObservable();


    public void loadStart() {
        loadData(ProgressType.ListProgress, 0, START_PAGE_SIZE);
    }

    public void loadNext(int offset) {
        loadData(ProgressType.Paging, offset, NEXT_PAGE_SIZE);
    }

    public void loadRefresh() {
        loadData(ProgressType.Refreshing, 0, START_PAGE_SIZE);
    }


    public void onLoadingStart(ProgressType progressType) {
        getViewState().onStartLoading();

        showProgress(progressType);
    }

    public void onLoadingFinish(ProgressType progressType) {
        mIsInLoading = false;

        getViewState().onFinishLoading();

        hideProgress(progressType);
    }

    public void onLoadingFailed(Throwable throwable) {
        getViewState().showError(throwable.getMessage());
    }


    public void onLoadingSuccess(ProgressType progressType, List<BaseViewModel> items) {
        if (progressType == ProgressType.Paging) {
            getViewState().addItems(items);
        } else {
            getViewState().setItems(items);
        }
    }


    public enum ProgressType {
        Refreshing, ListProgress, Paging
    }


    public void showProgress(ProgressType progressType) {
        switch (progressType) {
            case Refreshing:
                getViewState().showRefreshing();
                break;
            case ListProgress:
                getViewState().showListProgress();
                break;
        }
    }

    public void hideProgress(ProgressType progressType) {
        switch (progressType) {
            case Refreshing:
                getViewState().hideRefreshing();
                break;
            case ListProgress:
                getViewState().hideListProgress();
                break;
        }
    }


    public void saveToDb(RealmObject item) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> realm1.copyToRealmOrUpdate(item));
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }
}
