package info.fandroid.fandroidvk.mvp.view;

import com.arellomobile.mvp.MvpView;

import info.fandroid.fandroidvk.model.owner.Profile;
import info.fandroid.fandroidvk.ui.fragment.BaseFragment;

/**
 * Created by wet on 15.07.17.
 */

public interface HomeView extends MvpView {
    void startSignIn();

    void showScreenError(String message);

    void signedIn();
    void showCurrentUser(Profile profile);

    void showFragmentFromDrawer(BaseFragment baseFragment);

    void startActivityFromDrawer(Class<?> act);
}
