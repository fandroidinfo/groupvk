package info.fandroid.fandroidvk.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import info.fandroid.fandroidvk.CurrentUser;
import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.common.util.VkListHelper;
import info.fandroid.fandroidvk.model.WallItem;
import info.fandroid.fandroidvk.model.view.BaseViewModel;
import info.fandroid.fandroidvk.model.view.NewsItemBodyViewModel;
import info.fandroid.fandroidvk.model.view.NewsItemFooterViewModel;
import info.fandroid.fandroidvk.model.view.NewsItemHeaderViewModel;
import info.fandroid.fandroidvk.mvp.view.BaseFeedView;
import info.fandroid.fandroidvk.rest.api.ApiConstants;
import info.fandroid.fandroidvk.rest.api.WallApi;
import info.fandroid.fandroidvk.rest.model.request.WallGetRequestModel;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by wet on 15.07.17.
 */

@InjectViewState
public class NewsFeedPresenter extends BaseFeedPresenter<BaseFeedView> {

    @Inject
    WallApi mWallApi;

    private boolean enableIdFiltering = false;


    private static final String TAG = "NewsFeedPresenter";


    public NewsFeedPresenter() {
        MyApplication.getApplicationComponent().inject(this);
    }


    @Override
    public Observable<BaseViewModel> onCreateLoadDataObservable(int count, int offset) {
        return mWallApi.get(new WallGetRequestModel(ApiConstants.MY_GROUP_ID, count, offset).toMap())
                .flatMap(full -> Observable.fromIterable(VkListHelper.getWallList(full.response)))
                .compose(applyFilter())
                .doOnNext(this::saveToDb)
                .flatMap(wallItem -> Observable.fromIterable(parsePojoModel(wallItem)));
    }

    @Override
    public Observable<BaseViewModel> onCreateRestoreDataObservable() {
        return Observable.fromCallable(getListFromRealmCallable())
                .flatMap(Observable::fromIterable)
                .compose(applyFilter())
                .flatMap(wallItem -> Observable.fromIterable(parsePojoModel(wallItem)));
    }


    private List<BaseViewModel> parsePojoModel(WallItem wallItem) {
        List<BaseViewModel> baseItems = new ArrayList<>();
        baseItems.add(new NewsItemHeaderViewModel(wallItem));
        baseItems.add(new NewsItemBodyViewModel(wallItem));
        baseItems.add(new NewsItemFooterViewModel(wallItem));
        return baseItems;
    }


    public Callable<List<WallItem>> getListFromRealmCallable() {
        return () -> {
            String[] sortFields = {"isPinned", "date"};
            Sort[] sortOrder = {Sort.DESCENDING, Sort.DESCENDING};
            Realm realm = Realm.getDefaultInstance();
            RealmResults<WallItem> realmResults = realm.where(WallItem.class)
                    .equalTo("isRepost", false)
                    .findAllSorted(sortFields, sortOrder);
            return realm.copyFromRealm(realmResults);
        };
    }


    public void setEnableIdFiltering(boolean enableIdFiltering) {
        this.enableIdFiltering = enableIdFiltering;
    }

    protected ObservableTransformer<WallItem, WallItem> applyFilter() {
        if (enableIdFiltering && CurrentUser.getId() != null) {
            return baseItemObservable -> baseItemObservable.
                    filter(wallItem -> CurrentUser.getId().equals(String.valueOf(wallItem.getSenderId())));
        } else {
            return baseItemObservable -> baseItemObservable;
        }
    }
}
