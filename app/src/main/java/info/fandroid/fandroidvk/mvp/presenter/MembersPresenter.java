package info.fandroid.fandroidvk.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.model.owner.Member;
import info.fandroid.fandroidvk.model.view.BaseViewModel;
import info.fandroid.fandroidvk.model.view.ProfileViewModel;
import info.fandroid.fandroidvk.mvp.view.BaseFeedView;
import info.fandroid.fandroidvk.rest.api.ApiConstants;
import info.fandroid.fandroidvk.rest.api.GroupsApi;
import info.fandroid.fandroidvk.rest.model.request.GroupsGetMembersRequestModel;
import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by wet on 20.07.17.
 */

@InjectViewState
public class MembersPresenter extends BaseFeedPresenter<BaseFeedView> {

    @Inject
    GroupsApi mGroupApi;

    public MembersPresenter() {
        MyApplication.getApplicationComponent().inject(this);
    }

    @Override
    public Observable<BaseViewModel> onCreateLoadDataObservable(int count, int offset) {
        return mGroupApi.getMembers(new GroupsGetMembersRequestModel(
                ApiConstants.MY_GROUP_ID, count, offset).toMap())
                .flatMap(baseItemResponseFull -> {
                    return Observable.fromIterable(baseItemResponseFull.response.getItems());
                })
                .doOnNext(member -> member.setGroupId(ApiConstants.MY_GROUP_ID))
                .doOnNext(member -> saveToDb(member))
                .map(member -> new ProfileViewModel(member));
    }

    @Override
    public Observable<BaseViewModel> onCreateRestoreDataObservable() {
        return Observable.fromCallable(getListFromRealmCallable())
                .flatMap(Observable::fromIterable)
                .map(member -> new ProfileViewModel(member));
    }



    public Callable<List<Member>> getListFromRealmCallable() {
        return () -> {
            String[] sortFields = {Member.ID};
            Sort[] sortOrder = {Sort.ASCENDING};

            Realm realm = Realm.getDefaultInstance();
            RealmResults<Member> results = realm.where(Member.class)
                    .equalTo("groupId", ApiConstants.MY_GROUP_ID)
                    .findAllSorted(sortFields, sortOrder);
            return realm.copyFromRealm(results);
        };
    }
}
