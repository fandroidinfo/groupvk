package info.fandroid.fandroidvk.mvp.view;

import com.arellomobile.mvp.MvpView;

import info.fandroid.fandroidvk.model.WallItem;
import info.fandroid.fandroidvk.model.view.counter.LikeCounterViewModel;

/**
 * Created by wet on 17.07.17.
 */

public interface PostFooterView extends MvpView {
    void like(LikeCounterViewModel likes);

    void openComments(WallItem wallItem);
}
