package info.fandroid.fandroidvk.mvp.presenter;

import com.arellomobile.mvp.MvpPresenter;

import info.fandroid.fandroidvk.mvp.view.BaseView;

/**
 * Created by wet on 22.07.17.
 */

public class BasePresenter<V extends BaseView> extends MvpPresenter<V> {

}
