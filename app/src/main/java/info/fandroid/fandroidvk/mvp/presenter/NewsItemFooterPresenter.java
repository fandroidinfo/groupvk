package info.fandroid.fandroidvk.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.common.util.RxUtils;
import info.fandroid.fandroidvk.common.util.VkListHelper;
import info.fandroid.fandroidvk.model.WallItem;
import info.fandroid.fandroidvk.model.countable.Likes;
import info.fandroid.fandroidvk.model.view.counter.LikeCounterViewModel;
import info.fandroid.fandroidvk.mvp.view.PostFooterView;
import info.fandroid.fandroidvk.rest.api.LikeEventOnSubscribe;
import info.fandroid.fandroidvk.rest.api.WallApi;
import info.fandroid.fandroidvk.rest.model.request.WallGetByIdRequestModel;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

/**
 * Created by wet on 15.04.17.
 */

@InjectViewState
public class NewsItemFooterPresenter extends MvpPresenter<PostFooterView> {

    @Inject
    WallApi mWallApi;

    public NewsItemFooterPresenter() {
        MyApplication.getApplicationComponent().inject(this);
    }


    public void like(int postId) {
        WallItem wallItem = getWallItemFromRealm(postId);
        likeObservable(wallItem.getOwnerId(), wallItem.getId(), wallItem.getLikes())
                .compose(RxUtils.applySchedulers())
                .subscribe(likes -> {
                    getViewState().like(likes);
                }, error -> {
                    error.printStackTrace();
                });
    }


    public static final String POST = "post";

    public Observable<LikeCounterViewModel> likeObservable(int ownerId, int postId, Likes likes) {
        return Observable.create(new LikeEventOnSubscribe(POST, ownerId, postId, likes))

                .observeOn(Schedulers.io())
                .flatMap(count -> {

                    return mWallApi.getById(new WallGetByIdRequestModel(ownerId, postId).toMap());
                })
                .flatMap(full -> Observable.fromIterable(VkListHelper.getWallList(full.response)))
                .map(wallItem -> new LikeCounterViewModel(wallItem.getLikes()));
    }

    public void openComments(int postId) {
        WallItem wallItem = getWallItemFromRealm(postId);
        getViewState().openComments(wallItem);
    }


    public WallItem getWallItemFromRealm(int postId) {
        Realm realm = Realm.getDefaultInstance();
        WallItem wallItem = realm.where(WallItem.class)
                .equalTo("id", postId)
                .findFirst();

        return realm.copyFromRealm(wallItem);
    }
}
