package info.fandroid.fandroidvk.fcm;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by user on 26.07.2017.
 */


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFMService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Handle data payload of FCM messages.
        Log.d(TAG, "FCM Message Id: " + remoteMessage.getMessageId());

        Log.d(TAG, "FCM Notification: " + remoteMessage.getNotification());
        Log.d(TAG, "FCM data: " + remoteMessage.getData());

        Log.d(TAG, "FCM Data Message: " + remoteMessage.getFrom());

        sendNotification(remoteMessage);


    }


    private void sendNotification(RemoteMessage remoteMessage) {
        PushModel pushModel = PushUtils.parseFcmMessage(remoteMessage.getData()).toPushModel();

        if (pushModel.getType().equals(FcmMessage.TYPE_REPLY)
                && !MyPreferencesManager.getInstance().getPushNotificationCommentReplies()) {
            return;
        }

        NotificationHelper.notify(
                this,
                pushModel);
    }

}



