package info.fandroid.fandroidvk.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by user on 26.07.2017.
 */


public class MyPreferencesManager {

    public static final String PREF_AUTH_SKIP = "pref_auth_skip";

    public static final String PREF_CONTENT_UPDATE = "pref_content_update";
    public static final String PREF_CONTENT_UPDATE_ON_OPEN = "pref_content_update_on_open";
    public static final String PREF_CONTENT_UPDATE_IF_EMPTY = "pref_content_update_if_empty";
    public static final String PREF_CONTENT_UPDATE_NEVER = "pref_content_update_never";

    public static final String PREF_PUSH_NOTIFICATION_ADMIN = "pref_push_notification_admin";
    public static final String PREF_PUSH_NOTIFICATION_MEMBERS = "pref_push_notification_members";
    public static final String PREF_PUSH_NOTIFICATION_COMMENT_REPLIES = "pref_push_notification_comment_replies";


    private Context mContext;
    private SharedPreferences mSharedPref;

    private static MyPreferencesManager ourInstance = new MyPreferencesManager();

    public static MyPreferencesManager getInstance() {
        return ourInstance;
    }

    public void init(Context context) {
        this.mContext = context;
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);

    }

    private MyPreferencesManager() {

    }


    public boolean getPrefAuthSkip() {
        return mSharedPref.getBoolean(PREF_AUTH_SKIP, false);
    }

    public String getPrefContentUpdate() {
        return mSharedPref.getString(PREF_CONTENT_UPDATE, PREF_CONTENT_UPDATE_ON_OPEN);
    }

    public boolean isPrefContentUpdateOnOpen() {
        return getPrefContentUpdate().equals(PREF_CONTENT_UPDATE_ON_OPEN);
    }

    public boolean isPrefContentUpdateIfEmpty() {
        return getPrefContentUpdate().equals(PREF_CONTENT_UPDATE_IF_EMPTY);
    }

    public boolean isPrefContentUpdateNever() {
        return getPrefContentUpdate().equals(PREF_CONTENT_UPDATE_NEVER);
    }

    public boolean getPushNotificationAdmin() {
        return mSharedPref.getBoolean(PREF_PUSH_NOTIFICATION_ADMIN, true);
    }

    public boolean getPushNotificationMembers() {
        return mSharedPref.getBoolean(PREF_PUSH_NOTIFICATION_MEMBERS, false);
    }

    public boolean getPushNotificationCommentReplies() {
        return mSharedPref.getBoolean(PREF_PUSH_NOTIFICATION_COMMENT_REPLIES, true);
    }
}