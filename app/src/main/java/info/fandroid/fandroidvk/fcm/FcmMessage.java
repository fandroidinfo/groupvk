package info.fandroid.fandroidvk.fcm;

/**
 * Created by user on 26.07.2017.
 */

public abstract class FcmMessage {
    public static final String TYPE = "type";
    public static final String FROM_ID = "from_id";
    public static final String TEXT = "text";

    public static final String TYPE_NEW_POST = "new_post";
    public static final String TYPE_REPLY = "reply";
    public static final String TYPE_COMMENT = "comment";

    protected String type;
    protected String fromId;
    protected String text;

    protected boolean valid;

    public boolean isValid() {
        return valid;
    }

    public abstract PushModel toPushModel();
}
