package info.fandroid.fandroidvk.fcm;

import android.support.annotation.DrawableRes;

import info.fandroid.fandroidvk.model.Place;

/**
 * Created by user on 26.07.2017.
 */



public class PushModel {

    public String type;

    public String title;
    public String text;

    @DrawableRes
    public int icon;

    public Place place;


    public PushModel(String type, String title, String text, int icon, Place place) {
        this.type = type;
        this.title = title;
        this.text = text;
        this.icon = icon;
        this.place = place;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }



    public void setPlace(Place place) {
        this.place = place;
    }

    public Place getPlace() {
        return place;
    }
}