package info.fandroid.fandroidvk.fcm;

import android.util.Log;

import java.util.Map;

import info.fandroid.fandroidvk.common.util.Utils;
import info.fandroid.fandroidvk.model.Place;

/**
 * Created by user on 26.07.2017.
 */




public class ReplyFcmMessage extends FcmMessage {

    public static final String PLACE = "place";

    public String place;


    public ReplyFcmMessage(Map<String, String> source) {
        this.type = source.get(TYPE);
        this.fromId = source.get(FROM_ID);
        this.text = source.get(TEXT);
        this.place = source.get(PLACE);

        Log.d("reply", "source from id: " + source.get(FROM_ID));
        Log.d("reply", "from id: " + fromId);
    }


    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public PushModel toPushModel() {
        return new PushModel(this.type, null, this.text, 0, getPlace());
    }

    public Place getPlace() {
        String[] place = Utils.splitString(this.place);
        return new Place(place[0], place[1]);
    }
}




