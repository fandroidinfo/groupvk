package info.fandroid.fandroidvk.rest.model.response;

import java.util.ArrayList;
import java.util.List;

import info.fandroid.fandroidvk.model.owner.Group;
import info.fandroid.fandroidvk.model.owner.Owner;
import info.fandroid.fandroidvk.model.owner.Profile;

/**
 * Created by wet on 09.10.16.
 */

public class SenderResponse<T> extends BaseItemResponse<T> {

    public List<Profile> profiles = new ArrayList<>();
    public List<Group> groups = new ArrayList<>();

    public List<Profile> getProfiles() {
        return profiles;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public List<Owner> getAllSenders() {
        List<Owner> all = new ArrayList<>();
        all.addAll(getProfiles());
        all.addAll(getGroups());
        return all;
    }

    public Owner getSender(int id) {
        for (Owner owner : getAllSenders()) {
            if (owner.getId() == Math.abs(id)) {
                return owner;
            }
        }
        return null;
    }
}
