package info.fandroid.fandroidvk.rest.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wetalkas on 7/14/16.
 */
public class Full <T> {

    @SerializedName("response")
    @Expose
    public T response;

    @SerializedName("error")
    @Expose
    public ResponseError error;
}
