package info.fandroid.fandroidvk.rest.api;

import info.fandroid.fandroidvk.model.attachment.Audio;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by wet on 07.07.17.
 */

public interface AudioApi {

    @GET("audio.getById?v=5.52&{owner_id}_{audio_id}")
    Observable<Audio> getById(
            @Path("owner_id") String ownerId,
            @Path("audio_id") String audioId);
}
