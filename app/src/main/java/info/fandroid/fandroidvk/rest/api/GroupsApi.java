package info.fandroid.fandroidvk.rest.api;

import java.util.List;
import java.util.Map;

import info.fandroid.fandroidvk.model.owner.Group;
import info.fandroid.fandroidvk.model.owner.Member;
import info.fandroid.fandroidvk.rest.model.response.BaseItemResponse;
import info.fandroid.fandroidvk.rest.model.response.Full;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by wet on 09.07.17.
 */

public interface GroupsApi {
    @GET(ApiMethods.GROUPS_GET_BY_ID)
    Observable<Full<List<Group>>> getById(@QueryMap Map<String, String> map);

    @GET(ApiMethods.GROUPS_GET_MEMBERS)
    Observable<Full<BaseItemResponse<Member>>> getMembers(@QueryMap Map<String, String> map);
}
