package info.fandroid.fandroidvk.rest.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

import info.fandroid.fandroidvk.rest.api.ApiConstants;

/**
 * Created by wet on 09.07.17.
 */

public class GroupsGetByIdRequestModel extends BaseRequestModel {
    @SerializedName(ApiConstants.GROUP_ID)
    int groupId;

    @SerializedName(ApiConstants.FIELDS)
    String fields = ApiConstants.DEFAULT_GROUP_FIELDS;


    public GroupsGetByIdRequestModel(int groupId) {
        this.groupId = Math.abs(groupId);
    }


    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = Math.abs(groupId);
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }


    @Override
    public void onMapCreate(Map<String, String> map) {
        map.put(ApiConstants.GROUP_ID, String.valueOf(getGroupId()));
        map.put(ApiConstants.FIELDS, String.valueOf(getFields()));
    }
}
