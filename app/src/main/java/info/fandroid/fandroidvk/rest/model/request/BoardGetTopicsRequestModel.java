package info.fandroid.fandroidvk.rest.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

import info.fandroid.fandroidvk.rest.api.ApiConstants;

/**
 * Created by wet on 09.07.17.
 */

public class BoardGetTopicsRequestModel extends BaseRequestModel {
    @SerializedName(ApiConstants.GROUP_ID)
    int groupId;

    @SerializedName(ApiConstants.COUNT)
    int count = ApiConstants.DEFAULT_COUNT;

    @SerializedName(ApiConstants.OFFSET)
    int offset = 0;

    public BoardGetTopicsRequestModel(int groupId) {
        this.groupId = Math.abs(groupId);
    }

    public BoardGetTopicsRequestModel(int groupId, int count, int offset) {
        this.groupId = Math.abs(groupId);
        this.count = count;
        this.offset = offset;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = Math.abs(groupId);
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public void onMapCreate(Map<String, String> map) {
        map.put(ApiConstants.GROUP_ID, String.valueOf(getGroupId()));
        map.put(ApiConstants.COUNT, String.valueOf(getCount()));
        map.put(ApiConstants.OFFSET, String.valueOf(getOffset()));
    }
}
