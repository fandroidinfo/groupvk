package info.fandroid.fandroidvk.rest.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

import info.fandroid.fandroidvk.rest.api.ApiConstants;

/**
 * Created by wet on 09.07.17.
 */

public class WallGetByIdRequestModel extends BaseRequestModel {
    @SerializedName(ApiConstants.POSTS)
    String posts;

    @SerializedName(ApiConstants.EXTENDED)
    int extended = 1;

    public WallGetByIdRequestModel(int ownerId, int postId) {
        this.posts = ownerId + "_" + postId;
    }

    public String getPosts() {
        return posts;
    }

    public void setPosts(String posts) {
        this.posts = posts;
    }

    public int getExtended() {
        return extended;
    }

    public void setExtended(int extended) {
        this.extended = extended;
    }


    @Override
    public void onMapCreate(Map<String, String> map) {
        map.put(ApiConstants.POSTS, getPosts());
        map.put(ApiConstants.EXTENDED, String.valueOf(getExtended()));
    }
}