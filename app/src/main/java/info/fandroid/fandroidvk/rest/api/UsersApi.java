package info.fandroid.fandroidvk.rest.api;

import java.util.List;
import java.util.Map;

import info.fandroid.fandroidvk.model.owner.Profile;
import info.fandroid.fandroidvk.rest.model.response.Full;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by wet on 09.07.17.
 */

public interface UsersApi {
    @GET(ApiMethods.USERS_GET)
    Observable<Full<List<Profile>>> get(@QueryMap Map<String, String> map);
}
