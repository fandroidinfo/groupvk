package info.fandroid.fandroidvk.rest.api;

import java.util.Map;

import info.fandroid.fandroidvk.model.CommentItem;
import info.fandroid.fandroidvk.model.WallItem;
import info.fandroid.fandroidvk.rest.model.response.Full;
import info.fandroid.fandroidvk.rest.model.response.GetWallByIdResponse;
import info.fandroid.fandroidvk.rest.model.response.SenderResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by wet on 07.07.17.
 */

public interface WallApi {
    @GET(ApiMethods.WALL_GET)
    Observable<Full<SenderResponse<WallItem>>> get(@QueryMap Map<String, String> map);

    @GET(ApiMethods.WALL_GET_BY_ID)
    Observable<GetWallByIdResponse> getById(@QueryMap Map<String, String> map);

    @GET(ApiMethods.WALL_GET_COMMENTS)
    Observable<Full<SenderResponse<CommentItem>>> getComments(@QueryMap Map<String, String> map);
}
