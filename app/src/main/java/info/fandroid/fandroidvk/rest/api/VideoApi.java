package info.fandroid.fandroidvk.rest.api;

import java.util.Map;

import info.fandroid.fandroidvk.rest.model.response.Full;
import info.fandroid.fandroidvk.rest.model.response.VideosResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by wet on 09.07.17.
 */

public interface VideoApi {
    @GET(ApiMethods.VIDEO_GET)
    Observable<Full<VideosResponse>> get(@QueryMap Map<String, String> map);
}
