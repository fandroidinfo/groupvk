package info.fandroid.fandroidvk.rest.api;

import java.util.Map;

import info.fandroid.fandroidvk.model.Topic;
import info.fandroid.fandroidvk.rest.model.response.BaseItemResponse;
import info.fandroid.fandroidvk.rest.model.response.Full;
import info.fandroid.fandroidvk.rest.model.response.SenderResponse;
import info.fandroid.fandroidvk.model.CommentItem;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by wet on 09.07.17.
 */

public interface BoardApi {
    @GET(ApiMethods.BOARD_GET_TOPICS)
    Observable<Full<BaseItemResponse<Topic>>> getTopics(@QueryMap Map<String, String> map);

    @GET(ApiMethods.BOARD_GET_COMMENTS)
    Observable<Full<SenderResponse<CommentItem>>> getComments(@QueryMap Map<String, String> map);
}
