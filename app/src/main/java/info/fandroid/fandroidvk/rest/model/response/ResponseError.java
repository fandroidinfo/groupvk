package info.fandroid.fandroidvk.rest.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wetalkas on 7/5/16.
 */
public class ResponseError {
    @SerializedName("error_code")
    @Expose
    public int errorCode;
    @SerializedName("error_msg")
    @Expose
    public String errorMsg;
}
