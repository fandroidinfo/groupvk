package info.fandroid.fandroidvk.rest.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import info.fandroid.fandroidvk.model.attachment.video.Video;

/**
 * Created by wetalkas on 7/5/16.
 */
public class VideosResponse {
    public int count;
    @SerializedName("items")
    @Expose
    public List<Video> items;
}
