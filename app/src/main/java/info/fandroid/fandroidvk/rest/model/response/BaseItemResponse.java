package info.fandroid.fandroidvk.rest.model.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wet on 09.10.16.
 */

public class BaseItemResponse <T> {
    public Integer count;
    public List<T> items = new ArrayList<>();

    public Integer getCount() {
        return count;
    }

    public List<T> getItems() {
        return items;
    }
}
