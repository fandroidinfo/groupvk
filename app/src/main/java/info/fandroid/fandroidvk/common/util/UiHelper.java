package info.fandroid.fandroidvk.common.util;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.ColorInt;
import android.view.View;
import android.widget.TextView;

import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.model.countable.Countable;
import info.fandroid.fandroidvk.model.countable.Likes;

/**
 * Created by wetalkas on 7/23/16.
 */
public class UiHelper {
    private static UiHelper ourInstance = new UiHelper();

    private Resources resources;
    private Context context;

    public static UiHelper getInstance() {
        return ourInstance;
    }

    private UiHelper() {
    }

    public void init(Context context) {
        this.context = context;
        this.resources = context.getResources();
    }


    public void setUpCounter(Countable countable, TextView tvCounter, TextView tvCounterIcon) {
        boolean needAccent = false;

        if (countable instanceof Likes && ((Likes) countable).isUserLikes()) {
            needAccent = true;
        }
        if (countable != null) {

            setUpCounter(countable.getCount(), tvCounter, tvCounterIcon, needAccent);
        }
    }

    public void unbindCounter(TextView tvCounter, TextView tvCounterIcon) {

        tvCounter.setText(null);
        //tvCounterIcon.setText(null);
    }


    public void setUpCounter(int count, TextView tvCounter, TextView tvCounterIcon, boolean needAccent) {
        int color = R.color.colorIconDis;
        tvCounter.setText(" ");

        if (count > 0) {

            tvCounter.setText(String.valueOf(count));

            if (needAccent) {
                color = R.color.colorAccent;
            } else {
                color = R.color.colorIcon2;
            }

        }
        changeCounterColor(tvCounter, tvCounterIcon, resources.getColor(color));
    }



    public void changeCounterColor(TextView count, TextView icon, @ColorInt int colorId) {
        count.setTextColor(colorId);
        icon.setTextColor(colorId);
    }

    public void setUpTextViewWithVisibility(TextView textView, String s) {
        textView.setText(s);

        if (s.length() != 0) {
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    public void setUpTextViewWithMessage(TextView textView, String s, String messageIfEmpty) {
        String s1;
        int color;
        Resources res = textView.getResources();

        if (s.length() != 0) {
            textView.setVisibility(View.VISIBLE);
            color = android.R.color.primary_text_light;

            s1 = s;

        } else {
            s1 = "Поделился";
            color = R.color.colorIcon;
        }

        textView.setText(s1);
        textView.setTextColor(res.getColor(color));
    }
}
