package info.fandroid.fandroidvk.common.util;

import com.vk.sdk.api.model.VKAttachments;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import info.fandroid.fandroidvk.model.CommentItem;
import info.fandroid.fandroidvk.model.WallItem;
import info.fandroid.fandroidvk.model.attachment.ApiAttachment;
import info.fandroid.fandroidvk.model.owner.Owner;
import info.fandroid.fandroidvk.model.view.BaseViewModel;
import info.fandroid.fandroidvk.model.view.attachment.AudioAttachmentViewModel;
import info.fandroid.fandroidvk.model.view.attachment.DocAttachmentViewModel;
import info.fandroid.fandroidvk.model.view.attachment.DocImageAttachmentViewModel;
import info.fandroid.fandroidvk.model.view.attachment.ImageAttachmentViewModel;
import info.fandroid.fandroidvk.model.view.attachment.LinkAttachmentViewModel;
import info.fandroid.fandroidvk.model.view.attachment.LinkExternalViewModel;
import info.fandroid.fandroidvk.model.view.attachment.PageAttachmentViewModel;
import info.fandroid.fandroidvk.model.view.attachment.VideoAttachmentViewModel;
import info.fandroid.fandroidvk.rest.model.response.SenderResponse;

/**
 * Created by wetalkas on 7/1/16.
 */
public class VkListHelper {


    public static List<WallItem> getWallList(SenderResponse<WallItem> response) {
        List<WallItem> wallItems = response.items;

        for (WallItem wallItem : wallItems) {
            Owner sender = response.getSender(wallItem.getSenderId());
            wallItem.setIsRepost(false);
            wallItem.setSenderName(sender.getFullName());
            wallItem.setSenderPhoto(sender.getPhoto());

            wallItem.setAttachmentsString(Utils
                    .convertAttachmentsToFontIcons(wallItem.getAttachments()));

            if (wallItem.haveSharedRepost()) {
                Owner repostSender = response.getSender(wallItem.getSharedRepost().getSenderId());
                wallItem.getSharedRepost().setIsRepost(true);
                wallItem.getSharedRepost().setSenderName(repostSender.getFullName());
                wallItem.getSharedRepost().setSenderPhoto(repostSender.getPhoto());
                wallItem.getSharedRepost().setAttachmentsString(Utils
                        .convertAttachmentsToFontIcons(wallItem.getSharedRepost().getAttachments()));
            }
        }

        return wallItems;
    }


    public static List<CommentItem> getCommentsList(SenderResponse<CommentItem> response) {
        return getCommentsList(response, false);
    }

    public static List<CommentItem> getCommentsList(SenderResponse<CommentItem> response, boolean isFromTopic) {
        List<CommentItem> commentItems = response.items;

        for (CommentItem commentItem : commentItems) {
            Owner sender = response.getSender(commentItem.getFromId());
            commentItem.setSenderName(sender.getFullName());
            commentItem.setSenderPhoto(sender.getPhoto());

            commentItem.setIsFromTopic(isFromTopic);

            commentItem.setAttachmentsString(Utils
                    .convertAttachmentsToFontIcons(commentItem.getAttachments()));
        }
        return commentItems;
    }


    public static List<BaseViewModel> getAttachmentVhItems(List<ApiAttachment> attachments) {

        List<BaseViewModel> attachmentVhItems = new ArrayList<>();
        for (ApiAttachment attachment : attachments) {

            switch (attachment.getType()) {
                case VKAttachments.TYPE_PHOTO:
                    attachmentVhItems.add(new ImageAttachmentViewModel(attachment.getPhoto()));
                    break;

                case VKAttachments.TYPE_AUDIO:
                    attachmentVhItems.add(new AudioAttachmentViewModel(attachment.getAudio()));
                    break;

                case VKAttachments.TYPE_VIDEO:
                    attachmentVhItems.add(new VideoAttachmentViewModel(attachment.getVideo()));
                    break;

                case VKAttachments.TYPE_DOC:
                    if (attachment.getDoc().getPreview() != null) {
                        attachmentVhItems.add(new DocImageAttachmentViewModel(attachment.getDoc()));
                    } else {
                        attachmentVhItems.add(new DocAttachmentViewModel(attachment.getDoc()));
                    }
                    break;

                case VKAttachments.TYPE_LINK:
                    if (attachment.getLink().isExternal == 1) {
                        attachmentVhItems.add(new LinkExternalViewModel(attachment.getLink()));
                    } else {
                        attachmentVhItems.add(new LinkAttachmentViewModel(attachment.getLink()));
                    }
                    break;

                case "page":
                    attachmentVhItems.add(new PageAttachmentViewModel(attachment.getPage()));
                    break;

                default:
                    throw new NoSuchElementException("Attachment type " + attachment.getType() + " is not supported.");
            }
        }
        return attachmentVhItems;
    }

}
