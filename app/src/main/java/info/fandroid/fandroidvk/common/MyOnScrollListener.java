package info.fandroid.fandroidvk.common;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;

/**
 * Created by wet on 15.07.17.
 */

public class MyOnScrollListener extends RecyclerView.OnScrollListener {

    private FloatingActionButton mFab;

    private boolean mShowFab;

    private OnPageListener mOnPageListener;


    public interface OnPageListener {
        void onNextPage();
    }



    public MyOnScrollListener() {

    }

    public MyOnScrollListener(FloatingActionButton fab, boolean showFab) {
        mFab = fab;
        mShowFab = showFab;
    }


    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        checkFabVisibility(dx, dy);

        checkNextPage(dx, dy);
    }


    private void checkNextPage(int dx, int dy) {
        if (mOnPageListener != null) {
            mOnPageListener.onNextPage();
        }
    }



    private void checkFabVisibility(int dx, int dy) {
        if (mFab != null && mShowFab) {
            if (dy > 0)
                mFab.hide();
            else if (dy < 0)
                mFab.show();
        }
    }



    public void setOnPageListener(OnPageListener onPageListener) {
        mOnPageListener = onPageListener;
    }
}