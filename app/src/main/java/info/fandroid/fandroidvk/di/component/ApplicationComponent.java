package info.fandroid.fandroidvk.di.component;

import javax.inject.Singleton;

import dagger.Component;
import info.fandroid.fandroidvk.common.manager.MyFragmentManager;
import info.fandroid.fandroidvk.common.manager.NetworkManager;
import info.fandroid.fandroidvk.di.module.ApplicationModule;
import info.fandroid.fandroidvk.di.module.FontsModule;
import info.fandroid.fandroidvk.di.module.ManagerModule;
import info.fandroid.fandroidvk.di.module.RestModule;
import info.fandroid.fandroidvk.model.view.CommentBodyViewModel;
import info.fandroid.fandroidvk.model.view.CommentFooterViewModel;
import info.fandroid.fandroidvk.model.view.TopicViewModel;
import info.fandroid.fandroidvk.mvp.presenter.BoardPresenter;
import info.fandroid.fandroidvk.mvp.presenter.CommentsPresenter;
import info.fandroid.fandroidvk.mvp.presenter.HomePresenter;
import info.fandroid.fandroidvk.mvp.presenter.InfoPresenter;
import info.fandroid.fandroidvk.mvp.presenter.MembersPresenter;
import info.fandroid.fandroidvk.mvp.presenter.NewsFeedPresenter;
import info.fandroid.fandroidvk.mvp.presenter.NewsItemFooterPresenter;
import info.fandroid.fandroidvk.mvp.presenter.OpenedCommentPresenter;
import info.fandroid.fandroidvk.mvp.presenter.OpenedPostPresenter;
import info.fandroid.fandroidvk.mvp.presenter.TopicCommentsPresenter;
import info.fandroid.fandroidvk.ui.activity.BaseActivity;
import info.fandroid.fandroidvk.ui.activity.HomeActivity;
import info.fandroid.fandroidvk.ui.fragment.CommentsFragment;
import info.fandroid.fandroidvk.ui.fragment.OpenedCommentFragment;
import info.fandroid.fandroidvk.ui.fragment.OpenedPostFragment;
import info.fandroid.fandroidvk.ui.fragment.TopicCommentsFragment;
import info.fandroid.fandroidvk.ui.view.holder.NewsItemBodyHolder;
import info.fandroid.fandroidvk.ui.view.holder.NewsItemFooterHolder;
import info.fandroid.fandroidvk.ui.view.holder.attachment.ImageAttachmentHolder;
import info.fandroid.fandroidvk.ui.view.holder.attachment.VideoAttachmentHolder;

/**
 * Created by wet on 14.07.17.
 */
@Singleton
@Component(
        modules = {ApplicationModule.class, RestModule.class, ManagerModule.class, FontsModule.class})
public interface ApplicationComponent {

    //activities
    void inject(BaseActivity baseActivity);
    void inject(HomeActivity homeActivity);

    //fragments
    void inject(OpenedPostFragment fragment);
    void inject(CommentsFragment fragment);
    void inject(OpenedCommentFragment fragment);
    void inject(TopicCommentsFragment fragment);

    //managers
    void inject(MyFragmentManager myFragmentManager);
    void inject(NetworkManager networkManager);

    //presenters
    void inject(HomePresenter homePresenter);
    void inject(NewsFeedPresenter newsFeedPresenter);
    void inject(MembersPresenter membersPresenter);
    void inject(BoardPresenter boardPresenter);
    void inject(InfoPresenter infoPresenter);
    void inject(NewsItemFooterPresenter presenter);
    void inject(OpenedPostPresenter presenter);
    void inject(CommentsPresenter presenter);
    void inject(OpenedCommentPresenter presenter);
    void inject(TopicCommentsPresenter presenter);


    //viewholders
    void inject(NewsItemFooterHolder newsItemFooterHolder);
    void inject(NewsItemBodyHolder newsItemBodyHolder);
    void inject(CommentBodyViewModel.CommentBodyViewHolder holder);
    void inject(CommentFooterViewModel.CommentFooterHolder holder);
    void inject(TopicViewModel.TopicViewHolder holder);
    void inject(ImageAttachmentHolder holder);
    void inject(VideoAttachmentHolder holder);
}
