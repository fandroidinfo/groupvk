package info.fandroid.fandroidvk.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import info.fandroid.fandroidvk.rest.RestClient;
import info.fandroid.fandroidvk.rest.api.AccountApi;
import info.fandroid.fandroidvk.rest.api.AudioApi;
import info.fandroid.fandroidvk.rest.api.BoardApi;
import info.fandroid.fandroidvk.rest.api.GroupsApi;
import info.fandroid.fandroidvk.rest.api.UsersApi;
import info.fandroid.fandroidvk.rest.api.VideoApi;
import info.fandroid.fandroidvk.rest.api.WallApi;

/**
 * Created by wet on 15.07.17.
 */

@Module
public class RestModule {
    private RestClient mRestClient;


    public RestModule() {
        mRestClient = new RestClient();
    }


    @Provides
    @Singleton
    public RestClient provideRestClient() {
        return mRestClient;
    }


    @Provides
    @Singleton
    public AccountApi provideAccountApi() {
        return mRestClient.createService(AccountApi.class);
    }


    @Provides
    @Singleton
    public AudioApi provideAudioApi() {
        return mRestClient.createService(AudioApi.class);
    }

    @Provides
    @Singleton
    public BoardApi provideBoardApi() {
        return mRestClient.createService(BoardApi.class);
    }

    @Provides
    @Singleton
    public GroupsApi provideGroupsApi() {
        return mRestClient.createService(GroupsApi.class);
    }

    @Provides
    @Singleton
    public UsersApi provideUsersApi() {
        return mRestClient.createService(UsersApi.class);
    }

    @Provides
    @Singleton
    public VideoApi provideVideoApi() {
        return mRestClient.createService(VideoApi.class);
    }

    @Provides
    @Singleton
    public WallApi provideWallApi() {
        return mRestClient.createService(WallApi.class);
    }

}
