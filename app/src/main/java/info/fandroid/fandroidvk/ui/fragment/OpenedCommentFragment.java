package info.fandroid.fandroidvk.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindString;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.mvp.presenter.BaseFeedPresenter;
import info.fandroid.fandroidvk.mvp.presenter.OpenedCommentPresenter;

/**
 * Created by wet on 24.07.17.
 */

public class OpenedCommentFragment extends BaseFeedFragment {

    @BindString(R.string.screen_name_opened_comment)
    String mTitle;

    @InjectPresenter
    OpenedCommentPresenter mPresenter;

    int id;

    public static OpenedCommentFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt("id", id);
        OpenedCommentFragment fragment = new OpenedCommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getApplicationComponent().inject(this);

        if (getArguments() != null) {
            this.id = getArguments().getInt("id");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_opened_wall_item;
    }

    @Override
    public String getToolbarTitle() {
        return mTitle;
    }

    @Override
    protected BaseFeedPresenter onCreateFeedPresenter() {
        mPresenter.setId(id);
        return mPresenter;
    }
}
