package info.fandroid.fandroidvk.ui.view.holder.attachment;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.common.util.Utils;
import info.fandroid.fandroidvk.model.view.attachment.DocAttachmentViewModel;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;

/**
 * Created by wetalkas on 7/4/16.
 */
public class DocAttachmentHolder extends BaseViewHolder<DocAttachmentViewModel> {

    @BindView(R.id.tv_attachment_title)
    public TextView title;

    @BindView(R.id.tv_attachment_ext)
    public TextView ext;

    @BindView(R.id.tv_attachment_size)
    public TextView size;


    public DocAttachmentHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bindViewHolder(DocAttachmentViewModel docAttachmentViewModel) {
        if (docAttachmentViewModel.needClick) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utils.openUrlInActionView(docAttachmentViewModel.getmUrl(), view.getContext());
                }
            });
        }

        title.setText(docAttachmentViewModel.getTitle());

        size.setText(docAttachmentViewModel.getSize());

        ext.setText(docAttachmentViewModel.getExt());
    }

    @Override
    public void unbindViewHolder() {

        itemView.setOnClickListener(null);

        title.setText(null);

        size.setText(null);

        ext.setText(null);
    }




    /*@Override
    public void onFillViewHolder(Doc item) {
        if (item.getTitle().equals("")) {
            title.setText("Document");
        } else {
            title.setText(Utils.removeExtFromText(item.getTitle()));
        }

        size.setText(Utils.formatSize(item.getSize()));

        ext.setText("." + item.getExt());

    }


    @Override
    public void onItemClick(View v, Doc item) {
        UiNavigation.openUrlInActionView(item.getUrl(), mContext);
    }*/
}