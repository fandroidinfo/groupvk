package info.fandroid.fandroidvk.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindString;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.model.Place;
import info.fandroid.fandroidvk.mvp.presenter.BaseFeedPresenter;
import info.fandroid.fandroidvk.mvp.presenter.TopicCommentsPresenter;

/**
 * Created by wet on 24.07.17.
 */

public class TopicCommentsFragment extends BaseFeedFragment {

    public static TopicCommentsFragment newInstance(Place place) {

        Bundle args = new Bundle();
        args.putAll(place.toBundle());

        TopicCommentsFragment fragment = new TopicCommentsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @InjectPresenter
    TopicCommentsPresenter mPresenter;

    @BindString(R.string.screen_name_comments)
    String mTitle;

    Place mPlace;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getApplicationComponent().inject(this);
        withEndlessList(true);

        mPlace = new Place(getArguments());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public String getToolbarTitle() {
        return mTitle;
    }

    @Override
    protected BaseFeedPresenter onCreateFeedPresenter() {
        mPresenter.setPlace(mPlace);
        return mPresenter;
    }
}
