package info.fandroid.fandroidvk.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindString;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.mvp.presenter.BaseFeedPresenter;
import info.fandroid.fandroidvk.mvp.presenter.BoardPresenter;

/**
 * Created by wet on 22.07.17.
 */

public class BoardFragment extends BaseFeedFragment {

    @InjectPresenter
    BoardPresenter mPresenter;

    @BindString(R.string.screen_name_topics) String mTitle;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        withEndlessList(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    protected BaseFeedPresenter onCreateFeedPresenter() {
        return mPresenter;
    }

    @Override
    public String getToolbarTitle() {
        return mTitle;
    }
}
