package info.fandroid.fandroidvk.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.adapter.BaseAdapter;
import info.fandroid.fandroidvk.common.MyOnScrollListener;
import info.fandroid.fandroidvk.mvp.presenter.BaseFeedPresenter;
import info.fandroid.fandroidvk.mvp.view.BaseFeedView;
import info.fandroid.fandroidvk.common.manager.MyLinearLayoutManager;
import info.fandroid.fandroidvk.model.view.BaseViewModel;

/**
 * Created by wet on 15.07.17.
 */

public abstract class BaseFeedFragment extends BaseFragment implements BaseFeedView {

    protected RecyclerView mRecyclerView;

    protected BaseAdapter mAdapter;

    protected MyLinearLayoutManager mLinearLayoutManager;

    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected ProgressBar mProgressBar;

    protected BaseFeedPresenter mBaseFeedPresenter;

    private boolean isWithEndlessList;


    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_feed;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpUi();

        mBaseFeedPresenter = onCreateFeedPresenter();
        mBaseFeedPresenter.loadStart();
    }


    protected abstract BaseFeedPresenter onCreateFeedPresenter();


    @Override
    public void onResume() {
        super.onResume();
        mBaseFeedPresenter.loadRefresh();
    }



    protected void setUpUi() {
        setUpRecyclerView(mRootView);
        setUpAdapter(mRecyclerView);

        setUpSwipeToRefreshLayout(mRootView);
    }

    private void setUpSwipeToRefreshLayout(View rootView) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(() -> onCreateFeedPresenter().loadRefresh());

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        mProgressBar = getBaseActivity().getProgressBar();
    }

    private void setUpRecyclerView(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_list);

        MyOnScrollListener onScrollListener = new MyOnScrollListener(getBaseActivity().getFab(), needFab());
        mRecyclerView.addOnScrollListener(onScrollListener);

        mLinearLayoutManager = new MyLinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        if (isWithEndlessList) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    if (mLinearLayoutManager.isOnNextPagePosition()) {
                        mBaseFeedPresenter.loadNext(mAdapter.getRealItemCount());
                    }
                }
            });
        }

        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }


    public void withEndlessList(boolean withEndlessList) {
        isWithEndlessList = withEndlessList;
    }


    protected void setUpAdapter(RecyclerView rv) {
        mAdapter = new BaseAdapter();

        rv.setAdapter(mAdapter);
    }


    @Override
    public void showRefreshing() {
    }

    @Override
    public void hideRefreshing() {
        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void showListProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideListProgress() {
        mProgressBar.setVisibility(View.GONE);
    }


    @Override
    public void onStartLoading() {
    }

    @Override
    public void onFinishLoading() {
    }



    @Override
    public void showError(String message) {
        Toast.makeText(getBaseActivity(), message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void showEmptyScreen() {
    }

    @Override
    public void hideEmptyScreen() {
    }


    @Override
    public void setItems(List<BaseViewModel> items) {
        mAdapter.setItems(items);
    }

    @Override
    public void addItems(List<BaseViewModel> items) {
        mAdapter.addRange(items);
    }
}
