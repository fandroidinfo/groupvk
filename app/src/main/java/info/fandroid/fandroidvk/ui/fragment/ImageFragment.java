package info.fandroid.fandroidvk.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.R;

/**
 * Created by wet on 24.07.17.
 */

public class ImageFragment extends BaseFragment {

    @BindView(R.id.webview)
    WebView webView;

    @BindString(R.string.screen_name_image)
    String mTitle;


    public static ImageFragment newInstance(String url) {

        Bundle args = new Bundle();
        args.putString("url", url);

        ImageFragment fragment = new ImageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_group_rules;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setBackgroundColor(getResources().getColor(R.color.colorDefaultWhite));

        webView.loadUrl(getArguments().getString("url"));
    }

    @Override
    public String getToolbarTitle() {
        return mTitle;
    }


}
