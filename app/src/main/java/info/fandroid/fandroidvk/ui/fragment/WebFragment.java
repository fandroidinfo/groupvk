package info.fandroid.fandroidvk.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.R;

/**
 * Created by wet on 24.07.17.
 */

public class WebFragment extends BaseFragment {

    public static WebFragment newInstance(String url) {

        Bundle args = new Bundle();
        args.putString("url", url);

        WebFragment fragment = new WebFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String url;

    @BindView(R.id.webview)
    WebView mWebView;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);



        getBaseActivity().getProgressBar().setVisibility(View.VISIBLE);

        mWebView.loadUrl("http://www.fandroid.info/gruppa-vzaimopomoshhi-dlya-nachinayushhih-android-razrabotchikov-i-programmistov-osvoim-android-vmeste/");

        mWebView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                if (getBaseActivity() != null) {
                    getBaseActivity().getProgressBar().setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_group_rules;
    }

    @Override
    public String getToolbarTitle() {
        return null;
    }
}
