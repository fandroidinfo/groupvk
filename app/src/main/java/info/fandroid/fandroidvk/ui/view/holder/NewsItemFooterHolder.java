package info.fandroid.fandroidvk.ui.view.holder;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.common.manager.MyFragmentManager;
import info.fandroid.fandroidvk.common.util.RxUtils;
import info.fandroid.fandroidvk.common.util.Utils;
import info.fandroid.fandroidvk.common.util.VkListHelper;
import info.fandroid.fandroidvk.model.Place;
import info.fandroid.fandroidvk.model.WallItem;
import info.fandroid.fandroidvk.model.countable.Likes;
import info.fandroid.fandroidvk.model.view.NewsItemFooterViewModel;
import info.fandroid.fandroidvk.model.view.counter.CommentsCounterViewModel;
import info.fandroid.fandroidvk.model.view.counter.LikeCounterViewModel;
import info.fandroid.fandroidvk.model.view.counter.SharedCounterViewModel;
import info.fandroid.fandroidvk.mvp.presenter.NewsItemFooterPresenter;
import info.fandroid.fandroidvk.mvp.view.PostFooterView;
import info.fandroid.fandroidvk.rest.api.LikeEventOnSubscribe;
import info.fandroid.fandroidvk.rest.api.WallApi;
import info.fandroid.fandroidvk.rest.model.request.WallGetByIdRequestModel;
import info.fandroid.fandroidvk.ui.activity.BaseActivity;
import info.fandroid.fandroidvk.ui.fragment.CommentsFragment;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by wet on 01.05.17.
 */

public class NewsItemFooterHolder extends BaseViewHolder<NewsItemFooterViewModel> implements PostFooterView {

    @BindView(R.id.tv_date)
    public TextView tvDate;

    @BindView(R.id.rl_likes)
    public View rlLikes;

    @BindView(R.id.tv_likes_count)
    public TextView tvLikesCount;

    @BindView(R.id.tv_likes_icon)
    public TextView tvLikesIcon;


    @BindView(R.id.rl_comments)
    public View rlComments;

    @BindView(R.id.tv_comments_icon)
    public TextView tvCommentIcon;

    @BindView(R.id.tv_comments_count)
    public TextView tvCommentsCount;


    @BindView(R.id.tv_reposts_icon)
    public TextView tvRepostIcon;

    @BindView(R.id.tv_reposts_count)
    public TextView tvRepostsCount;

    @InjectPresenter
    NewsItemFooterPresenter presenter;

    @Inject
    Typeface mGoogleFontTypeface;

    @Inject
    MyFragmentManager mFragmentManager;

    @Inject
    WallApi mWallApi;

    private Resources mResources;

    private Context mContext;

    @ProvidePresenter
    NewsItemFooterPresenter provideRepositoryPresenter() {
        return new NewsItemFooterPresenter();
    }


    public NewsItemFooterHolder(View itemView) {
        super(itemView);
        MyApplication.getApplicationComponent().inject(this);
        ButterKnife.bind(this, itemView);

        mContext = itemView.getContext();
        mResources = mContext.getResources();

        tvLikesIcon.setTypeface(mGoogleFontTypeface);
        tvCommentIcon.setTypeface(mGoogleFontTypeface);
        tvRepostIcon.setTypeface(mGoogleFontTypeface);
    }


    public void like(NewsItemFooterViewModel item) {
        WallItem wallItem = getWallItemFromRealm(item.getId());
        likeObservable(wallItem.getOwnerId(), wallItem.getId(), wallItem.getLikes())
                .compose(RxUtils.applySchedulers())
                .subscribe(likes -> {
                    item.setmLikes(likes);
                    bindLikes(likes);
                }, error -> {
                    error.printStackTrace();
                });
    }


    public void saveToDb(RealmObject item) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> realm1.copyToRealmOrUpdate(item));
    }



    public static final String POST = "post";

    public Observable<LikeCounterViewModel> likeObservable(int ownerId, int postId, Likes likes) {
        return Observable.create(new LikeEventOnSubscribe(POST, ownerId, postId, likes))

                .observeOn(Schedulers.io())
                .flatMap(count -> {

                    return mWallApi.getById(new WallGetByIdRequestModel(ownerId, postId).toMap());
                })
                .flatMap(full -> Observable.fromIterable(VkListHelper.getWallList(full.response)))
                .doOnNext(this::saveToDb)
                .map(wallItem -> new LikeCounterViewModel(wallItem.getLikes()));
    }


    public WallItem getWallItemFromRealm(int postId) {
        Realm realm = Realm.getDefaultInstance();
        WallItem wallItem = realm.where(WallItem.class)
                .equalTo("id", postId)
                .findFirst();

        return realm.copyFromRealm(wallItem);
    }

    @Override
    public void bindViewHolder(NewsItemFooterViewModel item) {



        rlLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                like(item);
            }
        });
        //rlComments.setOnClickListener(view -> presenter.openComments(item.getId()));

        rlComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFragmentManager.addFragment((BaseActivity) view.getContext(),
                        CommentsFragment.newInstance(new Place(String.valueOf(item.getOwnerId()), String.valueOf(item.getId()))),
                        R.id.main_wrapper);
            }
        });

        tvDate.setText(Utils.parseDate(item.getDateLong(), mContext));

        bindLikes(item.getLikes());
        bindComments(item.getComments());
        bindReposts(item.getReposts());
    }

    private void bindLikes(LikeCounterViewModel likes) {
        tvLikesCount.setText(String.valueOf(likes.getCount()));
        tvLikesCount.setTextColor(mResources.getColor(likes.getTextColor()));
        tvLikesIcon.setTextColor(mResources.getColor(likes.getIconColor()));
    }

    private void bindComments(CommentsCounterViewModel comments) {
        tvCommentsCount.setText(String.valueOf(comments.getCount()));
        tvCommentsCount.setTextColor(mResources.getColor(comments.getTextColor()));
        tvCommentIcon.setTextColor(mResources.getColor(comments.getIconColor()));
    }

    private void bindReposts(SharedCounterViewModel reposts) {
        tvRepostsCount.setText(String.valueOf(reposts.getCount()));
        tvRepostsCount.setTextColor(mResources.getColor(reposts.getTextColor()));
        tvRepostIcon.setTextColor(mResources.getColor(reposts.getIconColor()));
    }


    @Override
    public void unbindViewHolder() {
        rlLikes.setOnClickListener(null);
        rlComments.setOnClickListener(null);

        tvDate.setText(null);

        tvLikesCount.setText(null);
        tvCommentsCount.setText(null);
        tvRepostsCount.setText(null);
    }

    @Override
    public void like(LikeCounterViewModel likes) {
        bindLikes(likes);
    }

    @Override
    public void openComments(WallItem wallItem) {
        //UiNavigation.openCommentsListInFragment((BaseActivity) itemView.getContext(), wallItem);
    }



}
