package info.fandroid.fandroidvk.ui.view.holder.attachment;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.common.manager.MyFragmentManager;
import info.fandroid.fandroidvk.model.view.attachment.ImageAttachmentViewModel;
import info.fandroid.fandroidvk.ui.activity.BaseActivity;
import info.fandroid.fandroidvk.ui.fragment.ImageFragment;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;

/**
 * Created by Vitaly on 09.02.2016.
 */
public class ImageAttachmentHolder extends BaseViewHolder<ImageAttachmentViewModel> {


    @BindView(R.id.iv_attachment_image)
    public ImageView image;

    @Inject
    MyFragmentManager mFragmentManager;


    public ImageAttachmentHolder(View itemView) {
        super(itemView);

        MyApplication.getApplicationComponent().inject(this);

        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bindViewHolder(ImageAttachmentViewModel imageAttachmentViewModel) {
        if (imageAttachmentViewModel.needClick) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mFragmentManager.addFragment((BaseActivity) itemView.getContext(),
                            ImageFragment.newInstance(imageAttachmentViewModel.getPhotoUrl()), R.id.main_wrapper);
                }
            });
        }

        Glide.with(itemView.getContext()).load(imageAttachmentViewModel.getPhotoUrl()).into(image);


    }

    @Override
    public void unbindViewHolder() {

        itemView.setOnClickListener(null);
        image.setImageBitmap(null);
    }


   /* @Override
    public void onFillViewHolder(Photo item) {

        if (item.getText().equals("")) {
            //name.setText("Photo");
        } else {
            //name.setText(photo.text);
        }

        Glide.with(mContext).load(item.getPhoto604()).into(image);
    }

    @Override
    public void unbindViewHolder() {
        image.setImageBitmap(null);
    }

    @Override
    public void onItemClick(View view, Photo item) {
        UiNavigation.openImageInFragment((BaseFragmentActivity) mContext, item.getPhoto1280());
    }*/
}
