package info.fandroid.fandroidvk.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindString;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.mvp.presenter.BaseFeedPresenter;
import info.fandroid.fandroidvk.mvp.presenter.MembersPresenter;

/**
 * Created by wet on 20.07.17.
 */

public class MembersFragment extends BaseFeedFragment {
    @InjectPresenter
    MembersPresenter mPresenter;

    @BindString(R.string.screen_name_members)
    String mTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        withEndlessList(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_feed;
    }

    @Override
    public String getToolbarTitle() {
        return mTitle;
    }

    @Override
    protected BaseFeedPresenter onCreateFeedPresenter() {
        return mPresenter;
    }
}
