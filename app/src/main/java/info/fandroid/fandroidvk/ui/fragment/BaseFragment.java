package info.fandroid.fandroidvk.ui.fragment;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;

import info.fandroid.fandroidvk.model.MyAnimation;
import info.fandroid.fandroidvk.ui.activity.BaseActivity;

/**
 * Created by wet on 14.07.17.
 */

public abstract class BaseFragment extends MvpAppCompatFragment {

    protected View mRootView;

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }



    @LayoutRes
    protected abstract int getMainContentLayout();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(getMainContentLayout(), container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbarTitle(getToolbarTitle());
        //getBaseActivity().setupFabVisibility(needFab());
    }

    public MyAnimation getAnimation() {
        return new MyAnimation();
    }

    private void setToolbarTitle(String title) {
        if (title != null && getBaseActivity() != null) {
            getBaseActivity().setToolbarTitle(title);
            //getBaseActivity().setupFabVisibility(true);
        }
    }


    public abstract String getToolbarTitle();

    public boolean needFab() {
        return false;
    }

    public void onScreen() {
        setToolbarTitle(getToolbarTitle());
        if (getBaseActivity() != null) {
            //getBaseActivity().setupFabVisibility(needFab());
        }
    }


}
