package info.fandroid.fandroidvk.ui.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.common.manager.MyFragmentManager;
import info.fandroid.fandroidvk.ui.fragment.BaseFragment;

/**
 * Created by wet on 14.07.17.
 */

public abstract class BaseActivity extends MvpAppCompatActivity {

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @BindView(R.id.progress)
    protected ProgressBar mProgressBar;

    @BindView(R.id.main_wrapper)
    FrameLayout mParent;

    @BindView(R.id.fab)
    public FloatingActionButton mFab;

    @Inject
    LayoutInflater mLayoutInflater;

    @Inject
    MyFragmentManager mMyFragmentManager;


    @LayoutRes
    protected abstract int getMainContentLayout();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);
        MyApplication.getApplicationComponent().inject(this);

        mLayoutInflater.inflate(getMainContentLayout(), mParent);
        setSupportActionBar(mToolbar);

        //setupFabVisibility(true);

    }

    public void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }


    public void setupFabVisibility(boolean needFab) {
        if (mFab == null) return;

        if (needFab) {
            mFab.show();
        } else {
            mFab.hide();
        }
    }

    public void setContent(BaseFragment fragment) {
        mMyFragmentManager.setFragment(this, fragment, R.id.main_wrapper);
    }

    public void addContent(BaseFragment fragment) {
        mMyFragmentManager.addFragment(this, fragment, R.id.main_wrapper);
    }

    public boolean removeCurrentFragment() {
        return mMyFragmentManager.removeCurrentFragment(this);
    }

    public boolean removeFragment(BaseFragment fragment) {
        return mMyFragmentManager.removeFragment(this, fragment);
    }

    @Override
    public void onBackPressed() {
        removeCurrentFragment();
    }


    public FloatingActionButton getFab() {
        return mFab;
    }

    public ProgressBar getProgressBar() {
        return mProgressBar;
    }

    public void onScreen(BaseFragment fragment) {
        setupFabVisibility(fragment.needFab());
    }
}
