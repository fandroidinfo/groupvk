package info.fandroid.fandroidvk.ui.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import info.fandroid.fandroidvk.model.view.BaseViewModel;

/**
 * Created by wet on 15.07.17.
 */

public abstract class BaseViewHolder<Item extends BaseViewModel> extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
        super(itemView);
    }


    public abstract void bindViewHolder(Item item);

    public abstract void unbindViewHolder();
}