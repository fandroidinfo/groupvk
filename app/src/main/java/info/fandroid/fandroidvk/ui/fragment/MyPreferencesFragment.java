package info.fandroid.fandroidvk.ui.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import info.fandroid.fandroidvk.R;

/**
 * Created by user on 26.07.2017.
 */

public class MyPreferencesFragment extends PreferenceFragment {


    public MyPreferencesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

    }/*

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }*/

}

