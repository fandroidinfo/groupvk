package info.fandroid.fandroidvk.ui.view.holder;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.common.manager.MyFragmentManager;
import info.fandroid.fandroidvk.common.util.UiHelper;
import info.fandroid.fandroidvk.model.view.NewsItemBodyViewModel;
import info.fandroid.fandroidvk.ui.activity.BaseActivity;
import info.fandroid.fandroidvk.ui.fragment.OpenedPostFragment;

/**
 * Created by wet on 17.07.17.
 */

public class NewsItemBodyHolder extends BaseViewHolder<NewsItemBodyViewModel> {

    @BindView(R.id.tv_text)
    public TextView tvText;

    @BindView(R.id.tv_attachments)
    public TextView tvAttachments;


    protected Typeface mFont;
    protected Typeface mFontGoogle;

    @Inject
    MyFragmentManager myFragmentManager;


    public NewsItemBodyHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        MyApplication.getApplicationComponent().inject(this);


        Context context = itemView.getContext();


        mFont = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        mFontGoogle = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");

        if (tvAttachments != null) {
            tvAttachments.setTypeface(mFontGoogle);
        }
    }

    @Override
    public void bindViewHolder(NewsItemBodyViewModel item) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myFragmentManager.addFragment((BaseActivity) view.getContext(),
                        OpenedPostFragment.newInstance(item.getId()),
                        R.id.main_wrapper);

            }
        });
        UiHelper.getInstance().setUpTextViewWithVisibility(tvText, item.getText());
        UiHelper.getInstance().setUpTextViewWithVisibility(tvAttachments, item.getAttachmentsString());
    }

    @Override
    public void unbindViewHolder() {
        itemView.setOnClickListener(null);
        tvText.setText(null);
        tvAttachments.setText(null);
    }
}

