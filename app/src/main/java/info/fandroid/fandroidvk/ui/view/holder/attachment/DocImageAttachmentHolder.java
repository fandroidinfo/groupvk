package info.fandroid.fandroidvk.ui.view.holder.attachment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.common.util.Utils;
import info.fandroid.fandroidvk.model.view.attachment.DocImageAttachmentViewModel;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;

/**
 * Created by wetalkas on 7/4/16.
 */
public class DocImageAttachmentHolder extends BaseViewHolder<DocImageAttachmentViewModel> {

    @BindView(R.id.tv_attachment_title)
    public TextView title;

    @BindView(R.id.tv_attachment_ext)
    public TextView ext;

    @BindView(R.id.tv_attachment_size)
    public TextView size;

    @BindView(R.id.iv_attachment_image)
    public ImageView image;


    public DocImageAttachmentHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bindViewHolder(DocImageAttachmentViewModel docImageAttachmentViewModel) {

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.openUrlInActionView(docImageAttachmentViewModel.getmUrl(), view.getContext());
            }
        });

        title.setText(docImageAttachmentViewModel.getTitle());

        size.setText(docImageAttachmentViewModel.getSize());

        ext.setText(docImageAttachmentViewModel.getExt());

        Glide.with(itemView.getContext()).load(docImageAttachmentViewModel.getImage()).into(image);
    }

    @Override
    public void unbindViewHolder() {
        itemView.setOnClickListener(null);
        title.setText(null);
        size.setText(null);
        ext.setText(null);
        image.setImageBitmap(null);
    }

    /*@Override
    public void onFillViewHolder(Doc item) {
        super.onFillViewHolder(item);

        List<Size> sizes = item.getPreview().getPhoto().getSizes();

        Glide.with(mContext).load(sizes.get(sizes.size() - 1).getSrc()).into(image);
    }*/
}