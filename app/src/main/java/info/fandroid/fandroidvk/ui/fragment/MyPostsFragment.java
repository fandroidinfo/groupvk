package info.fandroid.fandroidvk.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import butterknife.BindString;
import info.fandroid.fandroidvk.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPostsFragment extends NewsFeedFragment {

    @BindString(R.string.screen_name_my_posts)
    String mTitle;

    public MyPostsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.setEnableIdFiltering(true);
    }

    @Override
    public String getToolbarTitle() {
        return mTitle;
    }
}
