package info.fandroid.fandroidvk.ui.view.holder.attachment;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.model.view.attachment.AudioAttachmentViewModel;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;

/**
 * Created by Vitaly on 09.02.2016.
 */
public class AudioAttachmentHolder extends BaseViewHolder<AudioAttachmentViewModel> {

    @BindView(R.id.tv_audio_name)
    public TextView name;

    @BindView(R.id.tv_audio_artist)
    public TextView artist;

    @BindView(R.id.tv_audio_duration)
    public TextView duration;


    public AudioAttachmentHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bindViewHolder(AudioAttachmentViewModel audioAttachmentViewModel) {
        name.setText(audioAttachmentViewModel.getTitle());
        artist.setText(audioAttachmentViewModel.getArtist());

        duration.setText(audioAttachmentViewModel.getDuration());
    }

    @Override
    public void unbindViewHolder() {
        name.setText(null);
        artist.setText(null);

        duration.setText(null);
    }


    /*@Override
    protected void onFillViewHolder(Audio item) {
        if (item.getTitle().equals("")) {
            name.setText("Title");
        } else {
            name.setText(item.getTitle());
        }

        if (item.getArtist().equals("")) {
            artist.setText("Various Artist");
        } else {
            artist.setText(item.getArtist());
        }

        duration.setText(Utils.parseDuration(item.getDuration()));
    }*/


    /*@Override
    public void onItemClick(View v, Audio item) {
        UiNavigation.openAudioInActionView(item, mContext);
        Log.d("adapter position", this.getAdapterPosition() + "");
    }*/
}
