package info.fandroid.fandroidvk.ui.view.holder.attachment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.fandroid.fandroidvk.MyApplication;
import info.fandroid.fandroidvk.R;
import info.fandroid.fandroidvk.common.util.RxUtils;
import info.fandroid.fandroidvk.common.util.Utils;
import info.fandroid.fandroidvk.model.view.attachment.VideoAttachmentViewModel;
import info.fandroid.fandroidvk.rest.api.VideoApi;
import info.fandroid.fandroidvk.rest.model.request.VideoGetRequestModel;
import info.fandroid.fandroidvk.ui.view.holder.BaseViewHolder;
import io.reactivex.Observable;

/**
 * Created by Vitaly on 09.02.2016.
 */
public class VideoAttachmentHolder extends BaseViewHolder<VideoAttachmentViewModel> {

    @BindView(R.id.tv_attachment_video_title)
    public TextView title;

    @BindView(R.id.tv_attachment_video_duration)
    public TextView duration;

    @BindView(R.id.iv_attachment_video_picture)
    public ImageView image;

    @BindView(R.id.tv_views_count)
    public TextView views;

    @Inject
    VideoApi mVideoApi;

    public VideoAttachmentHolder(View itemView) {
        super(itemView);
        MyApplication.getApplicationComponent().inject(this);

        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bindViewHolder(VideoAttachmentViewModel videoAttachmentViewModel) {

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mVideoApi.get(new VideoGetRequestModel(videoAttachmentViewModel.getOwnerId(), videoAttachmentViewModel.getId()).toMap())

                        .flatMap(videosResponseFull -> Observable.fromIterable(videosResponseFull.response.items))
                        .compose(RxUtils.applySchedulers())
                        .subscribe(newVideo -> {
                            String url = newVideo.getFiles() == null ? newVideo.getPlayer() : newVideo.getFiles().getExternal();

                            Utils.openUrlInActionView(url, view.getContext());
                        });

            }
        });

        title.setText(videoAttachmentViewModel.getTitle());
        views.setText(videoAttachmentViewModel.getViewCount());
        duration.setText(videoAttachmentViewModel.getDuration());

        Glide.with(itemView.getContext()).load(videoAttachmentViewModel.getImageUrl()).into(image);
    }

    @Override
    public void unbindViewHolder() {
        itemView.setOnClickListener(null);

        title.setText(null);
        views.setText(null);
        duration.setText(null);

        image.setImageBitmap(null);
    }

/*

    @Override
    public void onFillViewHolder(Video video) {
        if (video.getTitle().equals("")) {
            title.setText("Video");
        } else {
            title.setText(video.getTitle());
        }

        views.setText(Utils.formatViewsCount(video.getViews()));

        duration.setText(Utils.parseDuration(video.getDuration()));

        Glide.with(mContext).load(video.getPhoto320()).into(image);
    }

    @Override
    public void onItemClick(View view, Video video) {
        UiNavigation.openVideoInActionView(video, mContext);
    }*/
}
